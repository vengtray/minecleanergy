<?php include("header.php"); ?>

    <div id="valuation">
        <div class="container">
            <div class="row">
                <div class="col-1 col-sm-1 col-md-1"></div>
                <div class="col-10 col-sm-10 col-md-10">
                    <div class="logo-img">
                        <img src="img/aoelogo.png" alt="alpha">
                    </div>
                    <div class="line"></div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h1>Valuations</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="text gold-font center">
                                <p>We would love to hear your thoughts in our survey! Help Us Help You!</p>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <form action="#" method="POST">
                                <div class="radio-form gold-font">
                                    <h3>With 1,704 and counting BREAKTHROUGH New Energy Technologies, how much would you value AOE stock price at?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q1" value="option1">
                                            <label for="q1">
                                                45x Earnings (typical Elephant late in the S curve Fang Stock "Best of SP500")
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q2" value="option2">
                                            <label for="q2">
                                                26-30x Earnings (SP500 USA Average biggest stocks prices, "Slower Growth")
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q3" value="option3">
                                            <label for="q3">
                                                92x Earnings (Recent Hong Kong Tech Stocks Prices) "Fast Growth Tech Stocks in Small Market"
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q4" value="option4">
                                            <label for="q4">
                                                137x Earnings (Recent Shenzhen Stock Market Tech Stocks Prices Average - South China Technology District) "Fast Growth Tech Stocks)
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q5" value="option5">
                                            <label for="q5">
                                                261x Earnings (Average Russel 2000 Stock "Fast Growth Global Small Companies")
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q6" value="option6">
                                            <label for="q6">
                                                333x-999x Earnings "Many Small-cap early stage IPOs with novel technology in developing or frontier markets with groundbreaking Value"
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How much did 1 single use-license of Ballard Fuel Cell Technology sell for recently?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q7" value="option1">
                                            <label for="q7">
                                                20 Million
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q8" value="option2">
                                            <label for="q8">
                                                30 Million
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q9" value="option3">
                                            <label for="q9">
                                                40 Million
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q10" value="option4">
                                            <label for="q10">
                                                50 million
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q11" value="option5">
                                            <label for="q11">
                                                What the heck is a fuel cell? You mean Apple and Google are not so dumb to still burn coal for electricity like others??!!
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="checkbox-form gold-font">
                                    <h3>Are you looking for a long term investment with stable high growth (10% plus) that has asset backing, some regular income to reinvest, or are you more of a flip daytrader?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q12" value="option1">
                                            <label for="q12">
                                                Flippy flip all the way, hopefully I don''t file bankruptcy one day!
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q13" value="option2">
                                            <label for="q13">
                                                I'm all about long term warren buffet style sleep at night never look for 10 years and it will still be there investments like AOECoin
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q14" value="option3">
                                            <label for="q14">
                                                I like buy and hold but I am lured by the greed side of crypto too much to invest in AOECoin
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q15" value="option4">
                                            <label for="q15">
                                                Love it, and will invest heavily
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>If One single license of Ballard Fuel Cell Tech sold for 50 million Dollars, and AOE has 1,704 techs that can be licensed in the future, How much is AOE and then AOECoin Worth?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q16" value="option1">
                                            <label for="q16">
                                                Wow its got to be in the hundreds of millions AT LEAST
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q17" value="option2">
                                            <label for="q17">
                                                BAHH who needs technology, technology is for the dogs!
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q18" value="option3">
                                            <label for="q18">
                                                Billions
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q19" value="option4">
                                            <label for="q19">
                                                Who cares, where is the BUYNOW page?
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>Will you be investing in the upcoming AOE IPO Also? (Initial Public Offering)</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q20" value="option1">
                                            <label for="q20">
                                                Yes, absolutely, I'm signing up for the updates for it!
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q21" value="option2">
                                            <label for="q21">
                                                What's an IPO?
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q22" value="option3">
                                            <label for="q22">
                                                I need help how to do it, then I'm in.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q23" value="option4">
                                            <label for="q23">
                                                I would like to sign up to be a Co-underwriter and Re-sell a block of AOE Shares.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q24" value="option5">
                                            <label for="q24">
                                                We are an Investment Bank, Professional Stock Fund Manager.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q25" value="option6">
                                            <label for="q25">
                                                We would like to report on it on our professional Media Channels.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How Crucially important do you feel it is for Media to report for free on these kinds of Gamechanging Small Private Innovators, especially those committed to the peaceful culture undeveloped Markets.</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q26" value="option1">
                                            <label for="q26">
                                                I feel it's so important that I am willing to buy some media space so that this gets more traction in raising investment sooner
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q27" value="option2">
                                            <label for="q27">
                                                I feel that all media, governments, NGOs, and Economic, business, and financial associations and organizations have a moral 
                                            </label>
                                            <label for="q27">&nbsp;&nbsp;&nbsp;&nbsp;and ethical total obligation to humanity to report on these kinds of people, companies, and missions.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q28" value="option3">
                                            <label for="q28">
                                                Nah, who cares, the Ad Sales Platform Networks don't have any obligation to anyone but money, let em do FAKE NEWS all day long, so what?
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q29" value="option4">
                                            <label for="q29">
                                                If they don't report and support, it's pretty much a basic litmus test of corruption.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>Do you think it's good for Real Businesses with Tokens and 'Token companies' to Also go IPO?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q30" value="option1">
                                            <label for="q30">
                                                Yes, because then the institutional investors can invest, and the company will be more legitimized, has capital to scale into large-cap global, 
                                            </label>
                                            <label for="q30">&nbsp;&nbsp;&nbsp;&nbsp;can attract many more investors, and will be pulled into strict financial compliance to all of us investors!</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q31" value="option2">
                                            <label for="q31">
                                                NO! You should have only a tiny amount of capital, a tiny raise pool, only have marketing to coin investors not the wall streets of the world, 
                                            </label>
                                            <label for="q31">&nbsp;&nbsp;&nbsp;&nbsp;and stay in the closet and never be a fully compliant regulated transparent and thus secure public institution that can include everyone </label>  
                                            <label for="q31">&nbsp;&nbsp;&nbsp;&nbsp;and get massive, just be a coin company and thats it!</label>                                     
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q32" value="option3">
                                            <label for="q32">
                                                I suck at choosing investments
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How much value do you think it will add to AOECoin when we add IPO?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question7" id="q33" value="option1">
                                            <label for="q33">
                                                Moon, moonage, nothing but moonOMania. Moonie, mooner, this is awesome.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question7" id="q34" value="option2">
                                            <label for="q34">
                                                AOECoin will probably sensibly increase in value and market cap to around the value of the market cap of the stock
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question7" id="q35" value="option3">
                                            <label for="q35">
                                                AOECoin will not only do above, but the total values of both will both increase and be higher than not having just one or the other also.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question7" id="q36" value="option4">
                                            <label for="q36">
                                                I think it will go to hell in a handbasket faster than a chinese solar panel company.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How much does AOE's Philanthropic Mission of For Profit Social Enterprise like Solar Orphanages, and Other Social Champion programs make you more likely to invest than other coins?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question8" id="q37" value="option1">
                                            <label for="q37">
                                                Influenced my decision at least to some level to buy more than not.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question8" id="q38" value="option2">
                                            <label for="q38">
                                                Huge influence on my decision
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question8" id="q39" value="option3">
                                            <label for="q39">
                                                 Doesn't influence my investment decision but I would rather you be doing it than not doing it that's for certain.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question8" id="q40" value="option4">
                                            <label for="q40">
                                            Make a contribution in other people's lives? Humph! Not responsible man!! Family should get all the money! We need to go to the mall!!! 
                                            </label>
                                            <label for="q40">&nbsp;&nbsp;&nbsp;&nbsp;So Irresponsible man!!! You will alone forever no girl will marry you!!</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>With "Climate Change" being a 500+ Billion Dollar a year spent industry, and 10 Trillion a year in 20 years, and World Economic forum stating its a 4.5 Trillion a year demand problem, what is the value of AOE's Breakthrough Technologies?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question9" id="q41" value="option1">
                                            <label for="q41">
                                                Wow man at least a few billion on their own
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question9" id="q42" value="option2">
                                            <label for="q42">
                                                At least 1 billion
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question9" id="q43" value="option3">
                                            <label for="q43">
                                                At least a few billion when they can install commercially and advertise that
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question9" id="q44" value="option4">
                                            <label for="q44">
                                                It aint worth nuthin! Technology doesn't even exist! Laws of physics LAWS OF PHYSICS you NERD!
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="submit-button">
                                    <input type="submit" class="btn btn-primary"></input>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-1 col-sm-1 col-md-1"></div>
            </div>
        </div>
    </div>

<?php include("footer.php"); ?>