<?php include("header.php"); ?>


<div id="science-survey">
    <div class="container">
        <div class="row">
            <div class="col-1 col-sm-1 col-md-1"></div>
            <div class="col-10 col-sm-10 col-md-10">
                <div class="logo-img">
                    <img src="img/aoelogo.png" alt="alpha">
                </div>
                <div class="line"></div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="title gold-font">
                            <h1>Science and Technology</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="text gold-font">
                            <p class="center">We would love to hear your thoughts or feedback on how we can improve your experience!</p>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <form action="#" method="POST">
                            <div class="radio-form gold-font">
                                <h3>What is at the Center of the Milky Way galaxy?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question" id="q1" value="option1">
                                        <label for="q1">
                                            A massive spiral disc collection of stars and gas that you can not see into because its so busy, cloudy, light, and full of material.
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question" id="q2" value="option2">
                                        <label for="q2">
                                            An absolute heap of Aliens that Nasa is hiding
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question" id="q3" value="option3">
                                        <label for="q3">
                                            That funny guy who got the Nobel prize for contradicting himself
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question" id="q4" value="option4">
                                        <label for="q4">
                                            The big black space fart
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question" id="q5" value="option5">
                                        <label for="q5">
                                            A huge completely empty strip that separates the entire galaxy into two halves, top and bottom, and it has nearly zero stars, matter, dust, clouds, 
                                        </label>
                                        <label for="q5">&nbsp;&nbsp;&nbsp;&nbsp;nor anything else strangely inside of it.</label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>What is at the Center of the Milky Way galaxy?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question1" id="q6" value="option1">
                                        <label for="q6">
                                            1000
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question1" id="q7" value="option2">
                                        <label for="q7">
                                            2000
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question1" id="q8" value="option3">
                                        <label for="q8">
                                            3000
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question1" id="q9" value="option4">
                                        <label for="q9">
                                            5000+
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="checkbox-form gold-font">
                                <h3>In what year was the world's most powerful battery to date patented, which still has received no support to date whatsoever and is practically not even in the market despite winning many awards?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="checkbox" name="checkbox-question" id="q10" value="option1">
                                        <label for="q10">
                                            2016    
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="checkbox" name="checkbox-question" id="q11" value="option2">
                                        <label for="q11">
                                            2014
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="checkbox" name="checkbox-question" id="q12" value="option3">
                                        <label for="q12">
                                            2013
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="checkbox" name="checkbox-question" id="q13" value="option4">
                                        <label for="q13">
                                            1985
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>In What Year are the majority of Fuel Cell Breakthroughs attributed to?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question2" id="q14" value="option1">
                                        <label for="q14">
                                            2016
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question2" id="q15" value="option2">
                                        <label for="q15">
                                            2015
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question2" id="q16" value="option3">
                                        <label for="q16">
                                            2009
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question2" id="q17" value="option4">
                                        <label for="q17">
                                            1979
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>In What Year was the first time Television Stations Filmed Nationally on TV, Cars that ran on nothing but water?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question3" id="q18" value="option1">
                                        <label for="q18">
                                            2016
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question3" id="q19" value="option2">
                                        <label for="q19">
                                            2014
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question3" id="q20" value="option3">
                                        <label for="q20">
                                            NEVER!! its BS!
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question3" id="q21" value="option4">
                                        <label for="q21">
                                            1983
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>In What year did Nikola Tesla Patent his Wireless Telegraphy (wireless radio signals) and wireless power system?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question4" id="q22" value="option1">
                                        <label for="q22">
                                            1979
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question4" id="q23" value="option2">
                                        <label for="q23">
                                            1945
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question4" id="q24" value="option3">
                                        <label for="q24">
                                            1921
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question4" id="q25" value="option4">
                                        <label for="q25">
                                            1901
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>What would Nikola Tesla say if He was here and found AOE with our 1,704 Breakthough Energy Techs?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question5" id="q26" value="option1">
                                        <label for="q26">
                                            Eureka Brother!! You've Done it!!
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question5" id="q27" value="option2">
                                        <label for="q27">
                                            DAMN! Now I have to redo a lot of patents!
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question5" id="q28" value="option3">
                                        <label for="q28">
                                            I can make this better with, Let me help you!
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question5" id="q29" value="option4">
                                        <label for="q29">
                                            GIMME THAT!! THAT'S NOT HOW YOU DO IT!!
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>What is "light"?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question6" id="q30" value="option1">
                                        <label for="q30">
                                            A particle
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question6" id="q31" value="option2">
                                        <label for="q31">
                                            A dielectric magnetic and electric signal
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question6" id="q32" value="option3">
                                        <label for="q32">
                                            A large single waveform that expands in every direction infinitely and expanding thus creating new energy over distance and time
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question6" id="q33" value="option4">
                                        <label for="q33">
                                            Some form of energy we have guessed at but still don't understand.
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>How does the sun make it's energy?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question7" id="q34" value="option1">
                                        <label for="q34">
                                            Scicom said core fusion a million times so I'm choosing to go with it
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question7" id="q35" value="option2">
                                        <label for="q35">
                                            A myraid of different ways not core fusion
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question7" id="q36" value="option3">
                                        <label for="q36">
                                            It's electrified / energized by Birkland currents
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question7" id="q37" value="option4">
                                        <label for="q37">
                                            It's a massive ball of highly radioactive material
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question7" id="q38" value="option6">
                                        <label for="q38">
                                            I have my own other theory
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>How much has the earth grown in size over the last 6,000 years from taking in energies of all kinds and also falling meteors?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question8" id="q39" value="option1">
                                        <label for="q39">
                                            4 times its size
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question8" id="q40" value="option2">
                                        <label for="q40">
                                            3 times its size
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question8" id="q41" value="option3">
                                        <label for="q41">
                                            2 times its size
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question8" id="q42" value="option4">
                                        <label for="q42">
                                            27.6% of its size
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>If there are 100 different applications in the energy industry, what is the prospect of commercializing better technology if Small Private Innovators were given funding?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question9" id="q43" value="option1">
                                        <label for="q43">
                                            All of them would be improved
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question9" id="q44" value="option2">
                                        <label for="q44">
                                            At least half of them would be improved
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question9" id="q45" value="option3">
                                        <label for="q45">
                                            BAH! I'm a Neo-Luddite, nothing could ever be made better, not even my poor eyesight and red freaking neck!
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question9" id="q46" value="option4">
                                        <label for="q46">
                                            Not only would all be improved but they would be absolutely smashed to the point of obsolescence.
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>How likely do you believe that Tesla's Wardenclyffe tower would have worked in lighting up the aurora borealis permenantly?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question10" id="q47" value="option1">
                                        <label for="q47">
                                            100% it will work if he said it would. Everything he did worked and everything we use and have to live on is from this guy,
                                        </label>
                                        <label for="q47">&nbsp;&nbsp;&nbsp;&nbsp;of course it would have worked!</label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question10" id="q48" value="option2">
                                        <label for="q48">
                                            Bah!! It's all a great big hoax mate! Let's smoke up the sky instead, it will glow! Just look at chiner!
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question10" id="q49" value="option3">
                                        <label for="q49">
                                            Why the hell doesn't someone resurrect this project?
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question10" id="q50" value="option4">
                                        <label for="q50">
                                            50-50 but I want to see it tried.
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>How many dimensions Does E=Mc2 measure correctly and fully?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question11" id="q51" value="option1">
                                        <label for="q51">
                                            1
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question11" id="q52" value="option2">
                                        <label for="q52">
                                            2
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question11" id="q53" value="option3">
                                        <label for="q53">
                                            3
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question11" id="q54" value="option4">
                                        <label for="q54">
                                            Hell if I know
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>How many dimensions Does E=Mc2 measure correctly and fully?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question12" id="q55" value="option1">
                                        <label for="q55">
                                            1
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question12" id="q56" value="option2">
                                        <label for="q56">
                                            2
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question12" id="q57" value="option3">
                                        <label for="q57">
                                            3
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question12" id="q58" value="option4">
                                        <label for="q58">
                                            Hell if I know
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>Nikola Tesla patented just over 700 Technologies in his life. AOE has 1,704 Techs to date and counting. How many techs does AOE need to design before you will consider Nikola is exceeded?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question13" id="q59" value="option1">
                                        <label for="q59">
                                            If you have 701 you have the win mate, lt alone 1,704
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question13" id="q60" value="option2">
                                        <label for="q60">
                                            If you have “the holy grail” in there, you got it, that’s enough right there for me.
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question13" id="q61" value="option3">
                                        <label for="q61">
                                            Nah, Tesla is the freakin’ man no matter what you got
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question13" id="q62" value="option4">
                                        <label for="q62">
                                            If you have 1,701 and have indeed exceeded and re-engineered Tesla’s techs near totally, you can be seriously considered a brother of this man 
                                        </label>
                                        <label for="q62">&nbsp;&nbsp;&nbsp;&nbsp;and Tesla would love you and kiss your face and say “Eureka Brother!!, You’ve Done it!”</label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>How strong is your belief in new radical energy technology development?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question14" id="q63" value="option1">
                                        <label for="q63">
                                            1
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question14" id="q64" value="option2">
                                        <label for="q64">
                                            2
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question14" id="q65" value="option3">
                                        <label for="q65">
                                            3
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question14" id="q66" value="option4">
                                        <label for="q66">
                                            Hell if I know
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="general-input gold-font">
                                <label for="InputName">Your Name</label>
                                <input type="text" class="form-control" id="InputName" placeholder="Enter your name">
                            </div>
                            <div class="email-form gold-font">
                                <label for="InputEmail">Email address</label>
                                <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Enter your email">
                                <small id="emailHelp" class="form-text">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="submit-button">
                                <input type="submit" class="btn btn-primary"></input>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-1 col-sm-1 col-md-1"></div>
        </div>
    </div>
</div>


<?php include("footer.php"); ?>