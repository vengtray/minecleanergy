<?php include "header.php"?>

    <div id="order">
        <div class="container">
            <div class="row">
               <div class="col-1 col-sm-1 col-md-1"></div>
               <div class="col-10 col-sm-10 col-md-10">
                    <div class="logo-img">
                        <img src="img/aoelogo.png" alt="alpha">
                    </div>
                    <div class="line"></div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h1>Order Electricity</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="text gold-font">
                                <p>After you fill out this order request, we will contact you to go over details and availability before the order is completed. We have a Large backorder so we encourage you to get into the Queue by filling out this form completely so we can keep track of the incoming order and priority needs of our clients.</p>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <form action="#" method="POST">
                                <div class="email-form gold-font">
                                    <label for="InputEmail">Email address</label>
                                    <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Enter your email">
                                    <small id="emailHelp" class="form-text">We'll never share your email with anyone else.</small>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>Are you a new or existing customer?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q1" value="option1">
                                            <label for="q1">
                                                I am a new client
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q2" value="option2">
                                            <label for="q2">
                                                I am an existing client
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="general-input gold-font">
                                    <label for="InputCountry">What country is your desired install facility in?</label>
                                    <input type="text" class="form-control" id="InputCountry" placeholder="Enter your country">
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>What is the service you would like to order?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q3" value="option1">
                                            <label for="q3">
                                                24 Hour Heavy Usage Demand = Fuel Cell Electricity Service
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q4" value="option2">
                                            <label for="q4">
                                                Daytime Only Usage = Solar Equipment Purchase
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q5" value="option3">
                                            <label for="q5">
                                                Daytime Only Usage = Solar Equipment Finance
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q6" value="option4">
                                            <label for="q6">
                                                Daytime Only Usage = Solar Equipment Rental
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q7" value="option5">
                                            <label for="q7">
                                                16 Hour Usage = Solar and Batteries Solution
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q8" value="option6">
                                            <label for="q8">
                                                We are Existing Developers/Investors with Capital seeking a large scale power plant
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q9" value="option7">
                                            <label for="q9">
                                                We are Goverment/Agency/ seeking power producers and are have/offering PPA’s (have documents)
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q10" value="option8">
                                            <label for="q10">
                                                Consumers seeking information for residential and other very small application or curiousity
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q11" value="option9">
                                            <label for="q11">
                                                Just curious
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="checkbox-form gold-font">
                                    <h3>How much is your electricity bill each month now?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q12" value="option1">
                                            <label for="q12">
                                                $100 or less
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q13" value="option2">
                                            <label for="q13">
                                                $101-150
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q14" value="option3">
                                            <label for="q14">
                                                $151-250
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q15" value="option4">
                                            <label for="q15">
                                                $251-450
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q16" value="option5">
                                            <label for="q16">
                                                $451-800
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q17" value="option6">
                                            <label for="q17">
                                                $800-1500
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q18" value="option7">
                                            <label for="q18">
                                                $1501 or more
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="general-input gold-font">
                                    <label for="InputFacility">What kind of facility is it?</label>
                                    <input type="text" class="form-control" id="InputFacility">
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>Is your buliding one of these types?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q19" value="option1">
                                            <label for="q19">
                                                Office Building
                                            </label>
                                            </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q20" value="option2">
                                            <label for="q20">
                                                Factory/processing plant
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q21" value="option3">
                                            <label for="q21">
                                                Bank/Financial company with data servers
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q22" value="option4">
                                            <label for="q22">
                                                Telecommunications Facility/Data Centers
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q23" value="option5">
                                            <label for="q23">
                                                Cold Food Storage/Warehousing/Food/Supermarkets
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q24" value="option6">
                                            <label for="q24">
                                                Wastewater treatment/pumping, irrigation, other 24 hour power demand infrastructure
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q25" value="option7">
                                            <label for="q25">
                                                Shopping Mall
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q26" value="option8">
                                            <label for="q26">
                                                Fire and Polic / Embassies / Goverment Facilities
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q27" value="option9">
                                            <label for="q27">
                                                Hospitals
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q28" value="option10">
                                            <label for="q28">
                                                Casino, 24 hour nightlife
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q29" value="option11">
                                            <label for="q29">
                                                Small Facility, restarant , petrol station, house, school , etc.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q30" value="option12">
                                            <label for="q30">
                                                Other, write what kind
                                            </label>
                                            <input type="text" class="form-control" id="q30">
                                        </div>
                                    </div>
                                </div>
                                <div class="general-input gold-font">
                                    <label for="InputBusiness">What sector is this business or company engaged in?</label>
                                    <input type="text" class="form-control" id="InputBusiness">
                                </div>
                                <div class="general-input gold-font">
                                    <label for="InputCompany">Company Name</label>
                                    <input type="text" class="form-control" id="InputCompany">
                                </div>
                                <div class="general-input gold-font">
                                    <label for="InputCompanySize">Company size in employees</label>
                                    <input type="text" class="form-control" id="InputCompanySize">
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How soon do you want your solution installed?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q31" value="option1">
                                            <label for="q31">
                                                As soon as physically possible
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q32" value="option2">
                                            <label for="q32">
                                                We need to solve our high cost energy problem and are ready for consultattion to learn, confirm and then proceed
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q33" value="option3">
                                            <label for="q33">
                                                We are just browsing for information and not actually doing anything
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="general-input gold-font">
                                    <label for="InputContact">Contact Info</label>
                                    <input type="text" class="form-control" id="InputContact">
                                </div>
                                <div class="general-input gold-font">
                                    <label for="InputName">Your Name</label>
                                    <input type="text" class="form-control" id="InputName">
                                </div>
                                <div class="general-input gold-font">
                                    <label for="InputNumber">Phone number</label>
                                    <input type="text" class="form-control" id="InputNumber">
                                </div>
                                <div class="checkbox-form gold-font">
                                    <h3>Preferred contact method</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question1" id="q34" value="option1">
                                            <label for="q34">
                                                Phone
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question1" id="q35" value="option2">
                                            <label for="q35">
                                                Email
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="general-input gold-font">
                                    <label for="InputAbout">Where did you hear about AOE?</label>
                                    <input type="text" class="form-control" id="InputAbout">
                                </div>
                                <div class="general-input gold-font">
                                    <label for="InputInformation">You can add information about your order here</label>
                                    <textarea row="10" type="text" class="form-control" id="InputInformation"> </textarea>
                                </div>
                                <div class="submit-button">
                                    <input type="submit" class="btn btn-primary"></input>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
               <div class="col-1 col-sm-1 col-md-1"></div>
            </div>
        </div>
    </div>

<?php include "footer.php"?>