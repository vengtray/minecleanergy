<?php include "header.php"?>

<div id="suggestion">
    <div class="container">
        <div class="row">
            <div class="col-1 col-sm-1 col-md-1"></div>
            <div class="col-10 col-sm-10 col-md-10">
                <div class="logo-img">
                    <img src="img/aoelogo.png" alt="alpha">
                </div>
                <div class="line"></div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="title gold-font">
                            <h1>Suggestions</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="text gold-font">
                            <p class="center">We would love to hear you thoughts or feedback on how we can improve your experience!</p>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <form action="#" method="POST">
                            <div class="general-input gold-font">
                                <label for="InputSuggestion">Write Your Suggestion!</label>
                                <textarea row="10" type="text" class="form-control" id="InputSuggestion"> </textarea>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>Feedback Type</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question" id="q1" value="option1">
                                        <label for="q1">
                                            Comments
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question" id="q2" value="option2">
                                        <label for="q2">
                                            Questions
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question" id="q3" value="option3">
                                        <label for="q3">
                                            Bug Reports
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question" id="q4" value="option4">
                                        <label for="q4">
                                            Feature Request
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>With 1,704 and counting BREAKTHROUGH New Energy Technologies, how much would you value AOE stock price at?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question1" id="q5" value="option1">
                                        <label for="q5">
                                            45x Earnings (typical Elephant late in the S curve Fang Stock "Best of SP500")
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question1" id="q6" value="option2">
                                        <label for="q6">
                                            26-30x Earnings (SP500 USA Average biggest stocks prices, "Slower Growth")
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question1" id="q7" value="option3">
                                        <label for="q7">
                                            92x Earnings (Recent Hong Kong Tech Stocks Prices) "Fast Growth Tech Stocks in Small Market"
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question1" id="q8" value="option4">
                                        <label for="q8">
                                            137x Earnings (Recent Shenzhen Stock Market Tech Stocks Prices Average - South China Technology District) "Fast Growth Tech Stocks)
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question1" id="q9" value="option5">
                                        <label for="q9">
                                            261x Earnings (Average Russel 2000 Stock "Fast Growth Global Small Companies")
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question1" id="q10" value="option6">
                                        <label for="q10">
                                            333x-999x Earnings "Many Small-cap early stage IPOs with novel technology in developing or frontier markets with groundbreaking Value"
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="checkbox-form gold-font">
                                <h3>How much do you plan on investing into AOECOIN?</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="checkbox" name="checkbox-question2" id="q11" value="option1">
                                        <label for="q11">
                                            $0-100
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="checkbox" name="checkbox-question2" id="q12" value="option2">
                                        <label for="q12">
                                            $101-500
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="checkbox" name="checkbox-question2" id="q13" value="option3">
                                        <label for="q13">
                                            $500-2000
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="checkbox" name="checkbox-question2" id="q14" value="option4">
                                        <label for="q14">
                                            $2,001-5000
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="checkbox" name="checkbox-question2" id="q15" value="option5">
                                        <label for="q15">
                                            $5,001-10,000
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="checkbox" name="checkbox-question2" id="q16" value="option6">
                                        <label for="q16">
                                            $10,001-25,000
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="checkbox" name="checkbox-question2" id="q17" value="option7">
                                        <label for="q17">
                                            $25,001-50,000
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="checkbox" name="checkbox-question2" id="q18" value="option8">
                                        <label for="q18">
                                            $50,001-99,999
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="checkbox" name="checkbox-question2" id="q19" value="option9">
                                        <label for="q19">
                                            Option 100,000+
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="checkbox" name="checkbox-question2" id="q20" value="option10">
                                        <label for="q20">
                                            Option 10
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>Will you be investing in the upcoming AOE IPO Also? (Initial Public Offering)</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question3" id="q21" value="option1">
                                        <label for="q21">
                                            Yes, absolutely, I'm signing up for the updates for it!
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question3" id="q22" value="option2">
                                        <label for="q22">
                                            What's an IPO?
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question3" id="q23" value="option3">
                                        <label for="q23">
                                            I need help how to do it, then I'm in.
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question3" id="q24" value="option4">
                                        <label for="q24">
                                            I would like to sign up to be a Co-underwriter and Re-sell a block of AOE Shares.
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question3" id="q25" value="option5">
                                        <label for="q25">
                                            We are an Investment Bank, Professional Stock Fund Manager.
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question3" id="q26" value="option6">
                                        <label for="q26">
                                            We would like to report on it on our professional Media Channels.
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>How Crucially important do you feel it is for Media to report for free on these kinds of Gamechanging Small Private Innovators, especially those committed to the peaceful culture undeveloped Markets.</h3>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question4" id="q27" value="option1">
                                        <label for="q27">
                                            I feel it's so important that I am willing to buy some media space so that this gets more traction in raising investment sooner
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question4" id="q28" value="option28">
                                        <label for="q28">
                                            I feel that all media, governments, NGOs, and Economic, business, and financial associations and organizations have a moral
                                        </label>
                                        <label for="q28">
                                        &nbsp;&nbsp;&nbsp;&nbsp;and ethical total obligation to humanity to report on these kinds of people, companies, and missions.
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question4" id="q29" value="option3">
                                        <label for="q29">
                                            Nah, who cares, the Ad Sales Platform Networks don't have any obligation to anyone but money, let em do FAKE NEWS all day long, so what?
                                        </label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question4" id="q30" value="option4">
                                        <label for="q30">
                                            If they don't report and support, it's pretty much a basic litmus test of corruption.
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>Rate us on our customer service</h3>
                                <div class="row center">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <label for="q31" class="space">1</label>
                                        <label for="q32" class="space">2</label>
                                        <label for="q33" class="space">3</label>
                                        <label for="q34" class="space">4</label>
                                        <label for="q35" class="space">5</label>
                                        <label for="q36" class="space">6</label>
                                        <label for="q37" class="space">7</label>
                                        <label for="q38" class="space">8</label>
                                        <label for="q39" class="space">9</label>
                                        <label for="q40" class="space">10</label>
                                    </div>
                                </div>
                                <div class="row center">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question5" id="q31" value="option1" class="space">
                                        <input type="radio" name="radio-question5" id="q32" value="option2" class="space">
                                        <input type="radio" name="radio-question5" id="q33" value="option3" class="space">
                                        <input type="radio" name="radio-question5" id="q34" value="option4" class="space">
                                        <input type="radio" name="radio-question5" id="q35" value="option5" class="space">
                                        <input type="radio" name="radio-question5" id="q36" value="option6" class="space">
                                        <input type="radio" name="radio-question5" id="q37" value="option7" class="space">
                                        <input type="radio" name="radio-question5" id="q38" value="option8" class="space">
                                        <input type="radio" name="radio-question5" id="q39" value="option9" class="space">
                                        <input type="radio" name="radio-question5" id="q40" value="option10" class="space">
                                    </div>
                                </div>
                                <div class="row center">
                                    <div class="col-4 col-sm-4 col-md-4">
                                        <h5>Needs Improvement</h5>
                                    </div>
                                    <div class="col-4 col-sm-4 col-md-4"></div>
                                    <div class="col-4 col-sm-4 col-md-4">
                                        <h5>So Good It's Electric</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>Rate our communication</h3>
                                <div class="row center">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <label for="q41" class="space">1</label>
                                        <label for="q42" class="space">2</label>
                                        <label for="q43" class="space">3</label>
                                        <label for="q44" class="space">4</label>
                                        <label for="q45" class="space">5</label>
                                        <label for="q46" class="space">6</label>
                                        <label for="q47" class="space">7</label>
                                        <label for="q48" class="space">8</label>
                                        <label for="q49" class="space">9</label>
                                        <label for="q50" class="space">10</label>
                                    </div>
                                </div>
                                <div class="row center">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question6" id="q41" value="option1" class="space">
                                        <input type="radio" name="radio-question6" id="q42" value="option2" class="space">
                                        <input type="radio" name="radio-question6" id="q43" value="option3" class="space">
                                        <input type="radio" name="radio-question6" id="q44" value="option4" class="space">
                                        <input type="radio" name="radio-question6" id="q45" value="option5" class="space">
                                        <input type="radio" name="radio-question6" id="q46" value="option6" class="space">
                                        <input type="radio" name="radio-question6" id="q47" value="option7" class="space">
                                        <input type="radio" name="radio-question6" id="q48" value="option8" class="space">
                                        <input type="radio" name="radio-question6" id="q49" value="option9" class="space">
                                        <input type="radio" name="radio-question6" id="q50" value="option10" class="space">
                                    </div>
                                </div>
                                <div class="row center">
                                    <div class="col-4 col-sm-4 col-md-4">
                                        <h5>Needs Improvement</h5>
                                    </div>
                                    <div class="col-4 col-sm-4 col-md-4"></div>
                                    <div class="col-4 col-sm-4 col-md-4">
                                        <h5>So Good It's Electric</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="radio-form gold-font">
                                <h3>Rate our Technology Portfolio</h3>
                                <div class="row center">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <label for="q51" class="space">1</label>
                                        <label for="q52" class="space">2</label>
                                        <label for="q53" class="space">3</label>
                                        <label for="q54" class="space">4</label>
                                        <label for="q55" class="space">5</label>
                                        <label for="q56" class="space">6</label>
                                        <label for="q57" class="space">7</label>
                                        <label for="q58" class="space">8</label>
                                        <label for="q59" class="space">9</label>
                                        <label for="q60" class="space">10</label>
                                    </div>
                                </div>
                                <div class="row center">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <input type="radio" name="radio-question7" id="q51" value="option51" class="space">
                                        <input type="radio" name="radio-question7" id="q52" value="option52" class="space">
                                        <input type="radio" name="radio-question7" id="q53" value="option53" class="space">
                                        <input type="radio" name="radio-question7" id="q54" value="option54" class="space">
                                        <input type="radio" name="radio-question7" id="q55" value="option55" class="space">
                                        <input type="radio" name="radio-question7" id="q56" value="option56" class="space">
                                        <input type="radio" name="radio-question7" id="q57" value="option57" class="space">
                                        <input type="radio" name="radio-question7" id="q58" value="option58" class="space">
                                        <input type="radio" name="radio-question7" id="q59" value="option59" class="space">
                                        <input type="radio" name="radio-question7" id="q60" value="option60" class="space">
                                    </div>
                                </div>
                                <div class="row center">
                                    <div class="col-4 col-sm-4 col-md-4">
                                        <h5>I don't believe in Technology, I live in a cave.</h5>
                                    </div>
                                    <div class="col-4 col-sm-4 col-md-4"></div>
                                    <div class="col-4 col-sm-4 col-md-4">
                                        <h5>Holy Crap I'm Tingling, You are going to CHANGE THE WORLD!</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="general-input gold-font">
                                <label for="InputName">Your Name</label>
                                <input type="text" class="form-control" id="InputName" placeholder="Your Name">
                            </div>
                            <br/>
                            <div class="email-form gold-font">
                                <label for="InputEmail">Email address</label>
                                <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Enter your email">
                                <small id="emailHelp" class="form-text">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="submit-button">
                                <input type="submit" class="btn btn-primary"></input>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-1 col-sm-1 col-md-1"></div>
        </div>
    </div>
</div>

<?php include "footer.php"?>