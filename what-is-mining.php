<?php include("header.php"); ?>


    <div id="what-is-mining">
        <div class="container">
            <div class="row">
                <div class="col-1 col-sm-1 col-md-1"></div>
                <div class="col-10 col-sm-10 col-md-10">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font center">
                                <h3>WHAT IS CLOUD MINING?</h3>
                            </div>
                            <div class="desc gold-font">
                                <p>Cloud (or remote) mining – is the process of using hardware power to mine cryptocurrency (such as
                                    Bitcoin or Litecoin) remotely. This mining model came to existence due to the fact that the increasing
                                    difficulty of mining has made it unprofitable for mining enthusiasts to mine Bitcoins at home.</p>
                                <p>Cloud mining gives people a unique opportunity to begin mining cryptocurrency without the need for
                                    a large initial investment in hardware or technical knowledge. Despite the simplicity of the
                                    cloud mining model, it is worth elaborating on a few details, specifically it’s important to
                                    highlight that remote mining comes in two forms: hosted or cloud based mining.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h3>REMOTE HOSTED MINING</h3>
                            </div>
                            <div class="desc gold-font">
                                <p>The first form of remote mining is remote hosting. This model suits users with a high level mining
                                    experience and know-how and who require a high degree of control over their mining hardware.
                                    Under this model, the mining hardware is hosted in a remote datacentre and the user assumes full
                                    control over the setup and configuration of the mining hardware. Under this model, the miner
                                    pays a fee to the hosting company that would cover maintenance and electricity costs. This helps
                                    the miner handle the risks associated with maintenance of the kit as well as any risks with the
                                    shipment of the hardware. On the other hand, it presents the miner with risk on the initial hardware
                                    investment and requires much more time and technical knowledge to implement successfully.</p>
                                <p>It can therefore be summarised that the benefits of remote hosted mining are tight control over the
                                    mining process, maintenance support and subsequent ownership of the hardware. The big drawbacks
                                    are risks associated with the procurement of expensive hardware and the very high cost of entry,
                                    both in terms of investment and technical experience.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h3>CLOUD MINING</h3>
                            </div>
                            <div class="desc gold-font">
                                <p>The second, and more accessible model of remote mining is Cloud Mining, whereby a miner purchases
                                    a part of the mining power of hardware hosted and owned by a Cloud Mining services provider.
                                    The service provider takes on configuring the hardware, maintaining uptime and selecting the
                                    most efficient and reliable pools.
                                </p>
                                <p>This option provides a range of benefits: instant connection (meaning no hardware shipment wait times
                                    and delivery risks), fixed maintenance and electricity fees and no nuisances associated with
                                    mining at home such as noise, heat or space. Another key point is that this model of cloud mining
                                    requires no technical experience. Obviously, it’s very important that miners understand the mining
                                    process, however this model doesn’t require hardware expertise or significant configuration /
                                    implementation cost. Since customers can purchase any amount of mining power they wish, this
                                    means that the level of investment will depend only on the miners’ ambition. This means that
                                    the cost of entry and subsequent risk is far lower than in comparison with the remotely hosted
                                    model.
                                </p>
                                <p>MineCleanergy is happy to be offering its new cloud mining services range. We guarantee an instant
                                    connection, around-the-clock access and monitoring, an easy-to-use management interface, 24/7
                                    uptime as well as daily payouts.</p>
                                <p>Cloud mining is greatly suited for novice miners who would like to try out mining and earning cryptocurrency
                                    as well as seasoned miners who don’t want the hassle or risks of hosted or home-based mining.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-1 col-sm-1 col-md-1"></div>
            </div>
        </div>
    </div>

<?php include("footer.php"); ?>