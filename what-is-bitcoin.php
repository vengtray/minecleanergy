<?php include("header.php"); ?>


    <div id="what-is-bitcoin">
        <div class="container">
           <div class="row">
               <div class="col-1 col-sm-1 col-md-1"></div>
               <div class="col-10 col-sm-10 col-md-10">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font center">
                                <h3>WHAT IS BITCION?</h3>
                            </div>
                            <div class="desc gold-font">
                                <p>Every country has its own currency. Estonia and the Eurozone have the Euro, Russia has the Rouble
                                    and the USA have US Dollars. We exchange money in banks or specialized foreign exchanges, transfer
                                    it worldwide and invest it. In our times of technological innovation it is safe to say that most,
                                    if not all money transfers are conducted through the Internet.</p>
                                <p>Is it then such a great surprise that an online currency such as Bitcoin has emerged? Interest in
                                    the currency has grown towards the end of 2013 due to significant spikes in the currency’s value,
                                    but let’s start from the beginning. In 2009, an unknown programmer by the name of Satoshi Nakamoto
                                    put forward a whitepaper that proposed a creation of new form of digital currency – cryptocurrency.
                                    Cryptocurrency functions the same way as regular currencies do in that its used as a means of
                                    exchange, unit of account and a store of value. Cryptocurrency, just like other resources, has
                                    some demand for it, and subsequently a market price. The significant difference is Bitcoin’s
                                    intangibility – there is no bank-issued notes or papers – meaning that rather being used in hand-to-hand
                                    transactions, Bitcoins are stored and exchanged digitally within a decentralized, peer-to-peer
                                    network.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h3>HOW DOES BITCOIN WORK?</h3>
                            </div>
                            <div class="desc gold-font">
                                <p>With traditional money, transferring funds from one account to another requires some intermediary
                                    authority or middleman. Even with hand-to-hand cash transactions, the issue, value and fiscal
                                    policy of money is controlled by a trusted centralized authority (such as a bank, agency or government).
                                    Bitcoin operates differently in that no middleman is required in transactions as the trust between
                                    actors is derived from computer science and cryptology, rather than trust in a central establishment.
                                    It also means that Bitcoin is transferred directly from the sender to the receiver, with absolutely
                                    no intermediaries.</p>
                                <p>A key point to note is that because of this lack of central issuing body, cryptocurrency is created
                                    and transferred with the help of a process called “mining”. This process requires an extremely
                                    powerful computer to crunch down the billions of calculations required to solve cryptological
                                    functions.
                                </p>
                                <p>In reality, the mining process is extremely complex and technical. Despite its complexity, the process
                                    is transparent and open for review due to the open-source nature of Bitcoin.</p>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h3>WHAT ARE THE STRENGTHS AND WEAKNESSES OF BITCOIN?</h3>
                            </div>
                            <div class="desc gold-font">
                                <p>Bitcoin is the first decentralized and uncontrolled currency. Since no central body owns the process
                                    for issuing new units, new coins are created at a fixed, predetermined rate. Unlike many government-issued
                                    currencies, this means that Bitcoin is immune from inflation, and is in fact a deflationary currency.
                                    Bitcoin also has the un unique property of “transparent anonymity”- meaning that despite all
                                    transactions and wallets being public through the Blockchain, all actors in a transaction are
                                    only identified by their bitcoin wallet address. Thousands of addresses are generated daily –
                                    this means that the user stays anonymous until they register both their personal details and
                                    their bitcoin wallet address somewhere (for example on a Bitcoin exchange). Bitcoin’s unique
                                    makeup also creates other strengths from the users perspective- the digital nature of Bitcoin
                                    makes it highly divisible and the lack of a central authority ensures that transaction fees are
                                    near-zero.
                                </p>
                                <p>Bitcoin’s digital nature and lack of central body also shape Bitcoin’s weaknesses - lost Bitcoins
                                    are non-recoverable (meaning that if you lose your private key or the hard drive with your wallet
                                    gets corrupted or if you lose your bitcoin wallet seed, those Bitcoins are lost forever!). Take
                                    the case of a British man, who in 2009, threw away the hard drive that contained his 7500 Bitcoins.
                                    At the end of 2013, the value of Bitcoin was nearing $1200, meaning there was a hard drive at
                                    a dump with over 8.25 million USD stored on it! Stories like this are not uncommon as early miners
                                    have been known to mine thousands of new coins, which would have made for a small fortune even
                                    with todays weak Bitcoin exchange rate.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h3>SO THEN, WHAT IS BITCOIN?</h3>
                            </div>
                            <div class="desc gold-font">
                                <p>Bitcoin is a peer-to-peer payment system. Its unique properties attract many followers and opponents.
                                </p>
                                <p>It’s impossible to tell whether Bitcoin will establish itself as the sole payment system for the
                                    internet, but for right now – there is significant interest and demand for it. The Bitcoin economy
                                    is still in its infancy and there are already many investors and people who are attracted by
                                    the prospects this new disrupting technology may bring.
                                </p>
                                <p>The future of Bitcoin is unclear at the moment due to legal uncertainty (governments cannot issue
                                    it, but they can prohibit use), unstable exchange rates and subsequent lack of widespread (albeit
                                    rapidly growing) adoption. However, people familiar with Bitcoin and technology often note the
                                    similarities between Bitcoin’s ascendance and the rise of Internet in the 1990-2000s. When the
                                    Internet first emerged in the early 90s many experts underestimated the impact it would have
                                    on the world. It is often predicted Bitcoin will follow the same pattern. Until then, current
                                    users are embracing this truly innovative idea and are contributing to establishing a global
                                    bitcoin economy.</p>
                            </div>
                        </div>
                    </div>
               </div>
               <div class="col-1 col-sm-1 col-md-1"></div>
           </div>
        </div>
    </div>

<?php include("footer.php"); ?>