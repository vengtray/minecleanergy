<?php 

    include('dbConfig.php'); 
    $conn = OpenCon();
    if(isset($_POST['submit']))
    {
        $first_name=$_POST['first_name'];
        $last_name=$_POST['last_name'];
        $gender=$_POST['gender'];
        if($gender=='male'){
            $gender='Male';
        }else{
            $gender='Female';
        }
        $user=$_POST['user_name'];
        $password=$_POST['password'];
        $confirm=$_POST['confirm_password'];
        $year=$_POST['year'];
        $month=$_POST['month'];
        $day=$_POST['day'];
        $date=$year."-".$month."-".$day; 
        $phone=$_POST['phone_number'];

        $sql = "INSERT INTO `numbers` (`id`,`first_name`,`last_name`,`gender`,`user_name`,`password`,`confirm_password`,`dob`,`phone_number`,'verification_code')
        VALUES ('','$first_name','$last_name','$gender','$user','$password','$confirm','$date','$phone','','')";

        $result = mysql_query($sql);
    }
    CloseCon($conn);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="img/Logo_new.ico" type="image/ico">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.11/css/intlTelInput.css" />
    <link href="css/style.css" rel="stylesheet">   

    <title>MineCleanergy</title>
</head>

<body class="main">

<div class="container">
    <!-- Navbar -->
    <div id="header">
        <div class="row">
            <div class="col-6 col-sm-6 col-md-6">
                <div class="logo-header">
                    <img src="img/minecleanergy.png" alt="">
                </div>
            </div>
            <div class="col-6 col-sm-6 col-md-6">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="index.php">HOME</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://aoecoin.io/buyaoecoin">BUY AOECOIN</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://aoecoin.io/videos" target="_blank">VIDEOS</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    ABOUT
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a href="http://aomegaenergy.tilda.ws/aboutus" class="dropdown-item" target="_blank">About AOE</a>
                                    <a href="https://aoecoin.io/" target="_blank" class="dropdown-item">About AOECOIN</a>
                                    <a href="https://ficleanergy.com/" target="_blank" class="dropdown-item">About FiCleanErgy</a>
                                    <a href="https://aoeangkor.com/" target="_blank" class="dropdown-item">About AOE Angkor Solar</a>
                                    <a class="dropdown-item" href="data-center.php">Our Data Center</a>
                                    <a href="http://aomegaenergy.tilda.ws/charity" class="dropdown-item" target="_blank">Charity</a>
                                    <a href="https://aoecoin.io/cambodia" class="dropdown-item" target="_blank">Cambodia</a>
                                    <a href="http://aomegaenergy.tilda.ws/champions" class="dropdown-item" target="_blank">Investor Champions</a>
                                    <a href="http://aomegaenergy.tilda.ws/tech" class="dropdown-item" target="_blank">Tech</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://forum.aoecoin.io/">FORUM</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    MORE
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="how-it-work.php">How It Works?</a>
                                    <a class="dropdown-item" href="what-is-bitcoin.php">What is Bitcoin?</a>
                                    <a class="dropdown-item" href="what-is-mining.php">What is Mining?</a>
                                    <a class="dropdown-item" href="faq.php">FAQ</a>
                                    <a class="dropdown-item" href="climate-section.php">Climate</a>
                                    <a class="dropdown-item" href="investment.php">Investment Education</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        
    <!-- End -->