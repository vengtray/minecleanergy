<?php include "header.php";?>

    <div id="faq">
        <div class="container">
            <div class="row">
                <div class="col-1 col-sm-1 col-md-1"></div>
                <div class="col-10 col-sm-10 col-md-10">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h3>Announements</h3>
                            </div>
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h4>CRYPTOCURRENCIES</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseTwo">
                                                Why Bitcoin Withdrawals and Payments Are Taking so Long to Confirm
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>If you have sent a bitcoin payment or requested a withdrawal of Bitcoin, you may have noticed that your transactions are taking much longer than usual to process.</p>
                                            <p>We are currently working through a backlog of ~18 thousand support emails, and we want to thank you for your patience. Since we have been receiving a large number of tickets about transaction confirmation delays, we are making this short announcement to explain, what is going on with the transaction confirmation time at the moment.</p>
                                            <p>Please understand, that MineCleanergy does not control or affect the transaction confirmation time directly in any way. Transactions on the Bitcoin network are grouped into blocks and added to the historical records of the blockchain by miners. High traffic on the blockchain and competition among users to have their transactions included in the next block will inevitably cause some transactions to get stuck in a queue for transaction confirmation. This queue is known as the Mempool. You can monitor the current size of the Mempool, as well as some other stats here. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                BTC - BIP148, Segwit2x, August 1st Fork
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>We recommend reading the following article in preparation to the upcoming and ongoing Bitcoin chain splits and the fork scheduled on August 1st: <a href="https://bitcoin.org/en/alert/2017-07-12-potential-split"> https://bitcoin.org/en/alert/2017-07-12-potential-split</a></p>
                                            <p>While the upcoming changes to Bitcoin will introduce radical improvements to the network, including Segwit <a href="https://bitcoincore.org/en/2016/01/26/segwit-benefits/">(https://bitcoincore.org/en/2016/01/26/segwit-benefits/)</a>, during this time it's important to keep your funds safe. For a period of time, Bitcoin confirmation score will become severely less reliable as some nodes of the network will be running nodes incompatible with others.</p>
                                            <p>To avoid risking customer funds and contracts during the events to come, MineCleanergy has decided to temporarily suspend all Bitcoin related operations:</p>
                                            <ul>
                                                <li>BTC withdrawals ;</li>
                                                <li>BTC purchases;</li>
                                                <li>SHA-256 mining contracts</li>
                                            </ul>
                                            <p>starting from 9 AM UTC on July 31, 2017, until the situation is resolved (most likely no later than August 4th).</p>
                                            <p><b>Also, please be aware that before and after the event,  Bitcoin confirmation score has become less reliable, which means it requires more time than usual to confirm and process Bitcoin transactions in the system.</b></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                ETH - Proof of Stake
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>The PoS (Proof of Stake) switch date is still unconfirmed, we will issue an official statement when the date is locked or more details are available.</p>
                                            <p>Your contract may be switched to mine another GPU-mineable coin, e.g. ZEC, ETC or XMR, depending on what's the most profitable coin at the time.</p>
                                            <p>More details on PoS can be found on <a href="https://github.com/ethereum/wiki/wiki/Proof-of-Stake-FAQ">Ethereum's GitHub page.</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h3>FAQ</h3>
                            </div>
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h4>GENERAL QUESTIONS</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingFour">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseTwo">
                                                What is MineCleanergy?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>MineCleanergy is a department of AOECOIN, a company that develops software for cloud mining and maintains equipment in datacenters</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFive">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseTwo">
                                                What cryptocurrencies can I mine with your service?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>MineCleanergy provides cloud mining on the following algorithms:</p>
                                            <ul>
                                                <li>SHA-256, which is used to mine Bitcoins;</li>
                                                <li>Scrypt, which is used to mine Litecoins*</li>
                                                <li>ETHASH, which is used to mine Ethereum;</li>
                                                <li>X11, which is used to mine DASH.</li>
                                                <li>*payouts are provided in BTC using the current exchange rate taken from Coinmarketcap.com.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingSix">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
                                                How long does the contract last?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>SHA-256 and SCRYPT contracts last 1 year(365 days) and are subject to maintenance and electricity fees (MEF)</p>
                                            <p>ETHASH, EQUIHASH and DASH contracts last for 1 year (365 days) and are not subject to any fees.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h4>FINANCIAL AND PROFITABILITY</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingSeven">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseTwo">
                                                How to calculate estimated profit using MineCleanergy?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>Profit is calculated via the deduction of expenses from income.</p>
                                            <p>The income consists of daily payouts which size depends on the hashrate. In order to calculate an estimated income using the hashrate you will need to include it in one of the calculators below (set all Power values to zero):</p>
                                            <p>1. <a href="https://www.coinwarz.com/calculators/bitcoin-mining-calculator" target="_blank">Bitcoin</a> - for SHA-256</p>
                                            <p>2. <a href="https://www.coinwarz.com/calculators/litecoin-mining-calculator" target="_blank">Litecoin</a> - for Scrypt</p>
                                            <p>3. <a href="https://www.coinwarz.com/calculators/ethereum-mining-calculator" target="_blank">Ethereum</a> - for ETHASH (set all Power values to zero)</p>
                                            <p>4. <a href="https://www.coinwarz.com/calculators/dash-mining-calculator" target="_blank">DASH</a> - for X11 (set all Power values to zero)</p>
                                            <p>5. <a href="https://www.coinwarz.com/calculators/zcash-mining-calculator" target="_blank">Zcash</a> - for EQUIHASH (set all Power values to zero)</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingEight">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseTwo">
                                                What influences my income and profit?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                                        <div class="card-body">
                                            <p><b>SHA-256</b> is used to mine Bitcoins (BTC). This means, that the only factor which influences the income is the mining difficulty.</p>
                                            <p><b>Scrypt</b> is used to mine Litecoins (LTC). This means, that there are 2 factors which influence the income:</p>
                                            <p>1. the difficulty of mining</p>
                                            <p>2. the exchange value of LTC/BTC</p>
                                            <p>Scrypt payouts are converted from LTC to BTC and then added to user's balance.</p>
                                            <p>SHA-256 and Scrypt contracts are also subject to daily maintenance+electricity fees (MEF) which are deducted from each payout. Currently the MEF is 0.0035 USD per 10 GH/s of SHA-256 and 0.005 USD per 1 MH/s of Scrypt.</p>
                                            <p>Once the MEF is deducted from a payout, the remaining amount is considered your profit.</p>
                                            <p>*BTC/USD exchange value also influences the MEF</p>
                                            <p><b>ETHASH</b> is used to mine Ethereum. As the payouts are provided in ETH, the only factor of influence is the difficulty of mining</p>
                                            <p><b>X11</b> is used for mining DASH. The main factor influencing the income is the mining difficulty</p>
                                            <p>ETHASH and X11 contracts are not subject to any additional fees.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingNine">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseThree">
                                                Maintenance and electricity fees (MEF)
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>The mining process requires energy consumption and human resources for the maintenance. In order to cover these fees all users are issued with maintenance and electricity fees (MEF) accordingly to their contract type and hashrate.</p>
                                            <p>The MEF is linear and equals 0.0035 USD per every 10 GH/s of SHA-256 and 0.005 USD per every 1 MH/s of Scrypt per day. Ethereum contracts are not subject to any fees. </p>
                                            <p>Maintenance and electricity fees are not billed in an invoice. Instead the fees are deducted from the customer's balance at the moment of daily payouts. Despite the fact that balance is accounted for in BTC and the fees - in USD, the fees are deducted in BTC according to the current USD/BTC exchange rate which is beneficial for the customer in case the exchange rate trend is positive - more BTC remains in the customer's balance.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTen">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseThree">
                                                Why am I unable to withdraw funds from my account balance?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>You can make 1 withdrawal per currency every 24 hours</p>
                                            <p>To ensure customer's account and balance security the system may automatically apply a hold, disallowing any withdrawals for the time the hold is active (14 days). There are multiple reasons for a hold to be applied:</p>
                                            <p>1. Change of a wallet address;</p>
                                            <p>2. Purchase made with a credit card;</p>
                                            <p>3. Change of password.</p>
                                            <p>4. Change of email address.</p>
                                            <p>In addition, if a referral makes their first ever purchase or a purchase using a credit card, the account of the referrer is placed on hold for 72 hours.</p>
                                            <p>Please note that technical support cannot remove the hold before it is automatically removed by the system. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingEleven">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseThree">
                                                Withdrawals: currencies and commission fees
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>Withdrawals for SHA-256 and Scrypt contracts are available in Bitcoin only.</p>
                                            <p>Minimum Bitcoin withdrawal amount is 0.050568 BTC BTC (incl. commission fees).</p>
                                            <p>Withdrawals for ETHASH contracts are available in Ethereum only.</p>
                                            <p>Minimum Ethereum withdrawal amount is 0.1006 ETH (incl. commission fees).</p>
                                            <p>Withdrawals for X11 contracts are available in DASH only.</p>
                                            <p>Minimum X11 amount is 0.1003 DASH (incl. commission fees).</p>
                                            <p>*you need to add an appropriate wallet to the system in order to make a withdrawal</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h4>WALLETS AND WITHDRAWALS</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingTwelve">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwo">
                                                How to create a Bitcoin wallet?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>The two general types of Bitcoin wallets are offline and online.</p>
                                            <p>Offline wallets store your bitcoins on a computer and they do not require an internet connection. However, such wallets require a lot of space on your hard drive and your constant attention (e.g. it is recommended to make backups weekly)</p>
                                            <p>An online wallet requires you to be connected to the Internet. You have to enter your login and password to access your wallet from any device and you don’t have to worry about its maintenance. </p>
                                            <p>If you choose to use an offline wallet, then we recommend to download and install Electrum. Run the program and follow the instructions. </p>
                                            <p>If you choose to use an online wallet, then sign up to CryptoPay. It provides the best security and utility among other online wallets. First, visit the signup page, enter your email address and choose a password. Second, check your inbox, look for a message from CryptoPay and confirm your account by clicking on the confirmation link in the message. All done! </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThirteen">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThirteen" aria-expanded="false" aria-controls="collapseTwo">
                                                How to create an Ethereum wallet?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>We recommend using the Mist Ethereum Wallet. You can download Windows, Linux and OS X versions from the developer's page here:<a href="https://github.com/ethereum/mist/releases" target="_blank"> https://github.com/ethereum/mist/releases </a></p>
                                            <p>Download the version for your operating system and install it. After installation, follow the instructions in the application to finish creating your wallet. </p>
                                            <p>Note: please, be aware that this wallet will download the Ethereum blockchain which is over 9 GB in size. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFourteen">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourteen" aria-expanded="false" aria-controls="collapseThree">
                                                How to create a DASH wallet?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFourteen" class="collapse" aria-labelledby="headingFourteen" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>We recommend using the Dash Core wallet, which is based on the well known Bitcoin Core QT wallet, but has a custom aesthetic. Anyone familiar with this one will be up and running with the Dash wallet in very little time.</p>
                                            <p>You can find a detailed article with download and installation instructions for Windows, Linux and OS X versions on dash.org</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFifteen">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFifteen" aria-expanded="false" aria-controls="collapseThree">
                                                Zcash address, withdrawals and wallet
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFifteen" class="collapse" aria-labelledby="headingFifteen" data-parent="#accordion">
                                        <div class="card-body">
                                            <p><b>Zcash</b> has two types of addresses: <b>z-addr</b> and <b>t-addr.</b> Z-addr uses zero-knowledge proofs and other means of protecting user privacy, while t-addr is similar to a Bitcoin address. At present, t-addr is used by most of the pools and exchanges as z-addr takes a lot of computing power to generate a transaction.</p>
                                            <p>MineCleanergy currently uses t-addresses for withdrawals.</p>
                                            <p>Largest cryptocurrency exchanges such as <a href="https://poloniex.com/"> Poloniex </a> and <a href="https://www.kraken.com/"> Kraken provide </a> t-addr for deposit and are the recommended option for withdrawing your funds.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h4>TECHNICAL QUESTIONS</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingSixteen">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSixteen" aria-expanded="false" aria-controls="collapseTwo">
                                                What is Reinvest? How do I use it?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseSixteen" class="collapse" aria-labelledby="headingSixteen" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>The Reinvest function allows you to automatically buy additional hashrate as soon as your Balance allows it. </p>
                                            <p>You can enable the Reinvest function in the Panel in the Balance block. Click "Reinvest" and select the type of hashrate. </p>
                                            <p>Activating this function will use your current Balance to purchase the maximum possible amount of hashrate.  </p>
                                            <p>After every payout, the system will check if your Balance is sufficient to purchase at least a minimum amount of hashrate (10 GH/s for SHA-256 or 1 MH/s for Scrypt). If it is, then the purchase will be created and automatically confirmed. </p>
                                            <p><b>NB!</b> Reinvest is available only for SHA256 and SCRYPT contracts. Unfortunately, you cannot reinvest into ETHASH, ZCASH and X11 contracts.</p>
                                            <p><b>How do I disable Reinvest?</b></p>
                                            <p>In order to disable the reinvest feature on your account please select <b>"Reinvest"</b> from your account's Dashboard, select <b>"Do not Reinvest"</b> from the drop-down menu and confirm the change via the <b>"Save"</b> button.</p>
                                            <p>Please note that the reinvest function is disabled by default. HashFlare technical support can not restore the balance if you have mistakenly activated the reinvest.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingSeventeen">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeventeen" aria-expanded="false" aria-controls="collapseTwo">
                                                How does pool allocation happen and work?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseSeventeen" class="collapse" aria-labelledby="headingSeventeen" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>Our equipment is set up to work with a number of pools. When you select a specific pool and the hashrate percent allocated to it, our system begins to use that amount of hashrate in the selected pool. The accounting happens daily, thus you are able to change the pools once a day. After the system calculates your hashrate's input and the total payout from that pool you will be given your share of the mined BTC</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingEighteen">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEighteen" aria-expanded="false" aria-controls="collapseThree">
                                                I did not get a confirmation email. What do I do?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseEighteen" class="collapse" aria-labelledby="headingEighteen" data-parent="#accordion">
                                        <div class="card-body">
                                        <p>If you haven’t received a confirmation within 10 minutes, please check your “Spam” folder. In case you don’t receive anything after 24 hours email us to <a href="mailto:ctw@aomegaenergy.com">ctw@aomegaenergy.com</a> </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-1 col-sm-1 col-md-1"></div>
            </div>
        </div>
    </div>

<?php include "footer.php";?>