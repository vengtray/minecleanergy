<?php include("header.php"); ?>

    <div id="economic">
        <div class="container">
            <div class="row">
                <div class="col-1 col-sm-1 col-md-1"></div>
                <div class="col-10 col-sm-10 col-md-10">
                    <div class="logo-img">
                        <img src="img/aoelogo.png" alt="alpha">
                    </div>
                    <div class="line"></div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h1>Economic Survey</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="text gold-font">
                                <p class="center">We would love to hear your thoughts or feedback on how we can improve your experience!</p>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <form action="#" method="POST">
                                <div class="radio-form gold-font">
                                    <h3>Who do you think should be directly investing cash in AOE, and who has the biggest obligation to invest cash directly into AOE the most?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question" id="q1" value="option1">
                                            <label for="q1">
                                                Governments
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question" id="q2" value="option2">
                                            <label for="q2">
                                                NGOs
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question" id="q3" value="option3">
                                            <label for="q3">
                                                The Uber Rich Like Bill Gates, Richard Branso, Elon Musk, and Climate change re-posters like Dicaprio and Hollywood
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question" id="q4" value="option4">
                                            <label for="q4">
                                                Development Banks
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>Scicom gets over 400 Billion a year in Government research funding, over 4 Trillion in the last 10 years alone in just the United States, and has zero Energy Breakthroughs and nearly zero commercialized new energy technologies or businesses created whatsoever. AOE is eligible for not one dollar of this funding yet has 1,709 Breakthrough New Energy Techs and already commercialzing. How much should AOE get per year instead of them?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q5" value="option1">
                                            <label for="q5">
                                                All of it
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q6" value="option2">
                                            <label for="q6">
                                                At least 50%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q7" value="option3">
                                            <label for="q7">
                                                100 Billion should do it for now
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q8" value="option4">
                                            <label for="q8">
                                                4 Billion. AOE could do 100X in results what these other guys have done with just 1% of what they get even ONCE let alone every year.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="checkbox-form gold-font">
                                    <h3>The Media refuses to report whatsoever on the Small Private Innovators and their Breakthrough Energy Technologies and guys like Bill Gates also refuse to support them, instead throwing another billion dollar useless firelog on the already 400+ Billion a year the USA gives to the completely defunct completely corrupt toxic-energy-crony-university complex of waste, misappropriation, near zero results and zero breakthroughs of any kind. What’s it going to take to get them to ever report on this?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q9" value="option1">
                                            <label for="q9">
                                                What? The FAKE NEWS media? Forget it bro, it’s all Pay to play, they couldn’t care less. 
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q10" value="option2">
                                            <label for="q10">
                                                Media is not media it’s just the “Ad Sales Business” and you have to pay them or they will just ignore you until you do.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q11" value="option3">
                                            <label for="q11">
                                                Go after the big media like Bloomberg and Specialty Channels like Prison Planet, they may actually care about this.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q12" value="option4">
                                            <label for="q12">
                                                I used to think the Ad Sales Business had some kind of responsibility to society but I can see they actually don’t give a crap, 
                                            </label>
                                            <label for="q12">&nbsp;&nbsp;&nbsp;&nbsp;it’s all about the money now. Endless clickbait and privacy prostitute dragnets like fakebook by suckerberg.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How big of a responsibility is it by governments to provide electricity to their people?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q13" value="option1">
                                            <label for="q13">
                                                All measures should be immediately implemented to solve this FIRST, by calling in all 239 of the other nations to build and profit
                                            </label>
                                            <label for="q13">&nbsp;&nbsp;&nbsp;&nbsp;in their complete freedom to do so with an open grid policy, and then everything else is second to energy because it all needs energy to run.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q14" value="option2">
                                            <label for="q14">
                                                Bah, who needs a clean running lightbulb? I have crap to light on fire…cough cough
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q15" value="option3">
                                            <label for="q15">
                                                There is no excuse that can be made other than corruption or total complete incompetance to not have enough clean energy for your people.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q16" value="option4">
                                            <label for="q16">
                                                Capitalism is my God and if people can’t solve these huge issues all by their tiny selves, to hell with them, Jesus, Buddah, and or 
                                            </label>
                                            <label for="q16">&nbsp;&nbsp;&nbsp;&nbsp;my reiki stick or even my mirror and my fakebook too, let them die in toxic breathing hell on earth, people have always been dying.</label>
                                            <label for="q16">&nbsp;&nbsp;&nbsp;&nbsp;(90% of this literally spoken to our faces by Gas company Executives in person in front of many other people)</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>Who is the right priority to give abundant clean energy and the focus of energy development to?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q17" value="option1">
                                            <label for="q17">
                                                The richest most immoral sleaze pit greedy selfish self worshipping hell on earth cultural icons like siliCON back alley.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q18" value="option2">
                                            <label for="q18">
                                                Other equally morally bankrupt cesspools of self-worship like Singapore, Hong Kong, Beijing, Shang….Any Chinese culture influenced city 
                                            </label>
                                            <label for="q18">&nbsp;&nbsp;&nbsp;&nbsp;for that matter, New York, Paris, … oh hey any “already rich with energy and money, big already developed have everything market” will do.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q19" value="option3">
                                            <label for="q19">
                                                Terrorist sponsor states and cultures and energy rich nations that should have developed way past the greed and
                                            </label>
                                            <label for="q19">&nbsp;&nbsp;&nbsp;&nbsp;charge models they slave all by.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q20" value="option4">
                                            <label for="q20">
                                                Poor and peaceful yet undeveloped, generally totally ignored, energy lack caused impoverished nations of underdogs with graceful peaceful 
                                            </label>
                                            <label for="q20">&nbsp;&nbsp;&nbsp;&nbsp;natures and cultures of humility and enlightenment with secure electricity lack percentages exceeding 60% of people?</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>What is the MINIMUM STANDARD for government officers to do to even BEGIN to claim or pretend as if they care whatsoever about climate change or energy development or breakthrough.</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q21" value="option1">
                                            <label for="q21">
                                                Invest $1 Million dollars directly to every small private innovator who even wants to have a crack at solving this 900 Trillion dollars 
                                            </label>
                                            <label for="q21">&nbsp;&nbsp;&nbsp;&nbsp;and Billion people dead in the next 100 years problem.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q22" value="option2">
                                            <label for="q22">
                                                Every global energy fair, event, and conference should focus on and feature the small private innovators, for free, and their breakthrough
                                            </label>
                                            <label for="q22">&nbsp;&nbsp;&nbsp;&nbsp;technologies and how to help them gain investment so that they can solve this issue for the world and create hundreds of millions of new jobs.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q23" value="option3">
                                            <label for="q23">
                                                The Grids must of course be 100% opened not 100% closed as they are now, so that anyone can plug in anything whatever it is they made that 
                                            </label>
                                            <label for="q23">&nbsp;&nbsp;&nbsp;&nbsp;makes electricity into the grid, and get paid the same price or even more than what retail customers pay for toxic energy, 100% hands off and</label>
                                            <label for="q23">&nbsp;&nbsp;&nbsp;&nbsp;no look at their IP and designs. Without this MINIMUM STANDARD of access, privacy, and security, no official nor spokesperson nor “media”</label>
                                            <label for="q23">&nbsp;&nbsp;&nbsp;&nbsp;(ad sales networks) can even pretend to claim that they somehow care about climate change nor the 10 million a year dead from air pollution nor</label>
                                            <label for="q23">&nbsp;&nbsp;&nbsp;&nbsp;in people having electricity.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q24" value="option4">
                                            <label for="q24">
                                                ALL OF THE ABOVE, 320 Million people will be killed from this in just the next 30 years alone, we should be at world war with anyone
                                            </label>

                                            <label for="q24">&nbsp;&nbsp;&nbsp;&nbsp;immediately who doesn’t act on this problem. Nukes should rain from the sky on any administration that doesn’t support this until one</label>
                                            <label for="q24">&nbsp;&nbsp;&nbsp;&nbsp;rises that does all these policies as they are systematically choosing the very deliberate other action of poisoning everyone leading to tens of</label>
                                            <label for="q24">&nbsp;&nbsp;&nbsp;&nbsp;millions of real human deaths every year.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>What is the MINIMUM STANDARD in order to claim that your business or that you, are Changing The World?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q25" value="option1">
                                            <label for="q25">
                                                You must put the most disadvantaged people in the world, that means the whole world or a poor, peaceful culture, and undeveloped,
                                            </label>
                                            <label for="q25">&nbsp;&nbsp;&nbsp;&nbsp;not just an Uber rich market, FIRST, and if you are ignoring them and doing nothing for them, you have no business claiming that you are</label>
                                            <label for="q25">&nbsp;&nbsp;&nbsp;&nbsp;supposedly changing the world.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q26" value="option2">
                                            <label for="q26">
                                                You can focus on apps, craps, virtual economy and virtual friendship and virtual reality and virtual relationship in the richest most developed 
                                            </label>
                                            <label for="q26">&nbsp;&nbsp;&nbsp;&nbsp;economies in the world in “rich-girl consumer type products” in the richest markets and or claim you are changing the world with A.I. while</label>
                                            <label for="q26">&nbsp;&nbsp;&nbsp;&nbsp;50% of people don’t even have access to secure electricity at all. That’s just fine like duh like whatever huh like wow man chill out bro like have</label>
                                            <label for="q26">&nbsp;&nbsp;&nbsp;&nbsp;another bong hit man look the poor people’s world is changed by my new supergirl shoes or my new pot plantation drugging up everyone’s</label>
                                            <label for="q26">&nbsp;&nbsp;&nbsp;&nbsp;minds. Wow like so hot like ill get like 1,000 more likes on my self-glorification fakebook selfies even!! This Changes Everything!</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q27" value="option3">
                                            <label for="q27">
                                                For-profit social enterprise in a viral model is the only real thing that will enact really enough change effect to begin to change the world,
                                            </label>
                                            <label for="q27">&nbsp;&nbsp;&nbsp;&nbsp;and without that all the rest of the talk is just BS at the top end of the socio-economic pyramid and actually creates even more disparity for</label>
                                            <label for="q27">&nbsp;&nbsp;&nbsp;&nbsp;people.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q28" value="option4">
                                            <label for="q28">
                                                Our business must break from the status quo near exception-less zombie pack of the core foundations of the mememelennial cultural
                                            </label>

                                            <label for="q28">&nbsp;&nbsp;&nbsp;&nbsp;generation of self-worship, greed, excess, self-glorification, instant gratification, fast click voiceless human touchless anti-social media,</label>
                                            <label for="q28">&nbsp;&nbsp;&nbsp;&nbsp;entitlement attitude, sleaze, regressive all the way 2000 years back to pagan-roman-era society in values, otherwise what the hell really are you</label>
                                            <label for="q28">&nbsp;&nbsp;&nbsp;&nbsp;changing? A fingernail color maybe, at best, regression cleverly called falsely as supposedly ‘progressive’.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How long do you think it will be before Scicom makes "fusion for energy?" (yet more toxic waste energy, cause it's not clean)</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q29" value="option1">
                                            <label for="q29">
                                                In 30 years (currently they claim it will take about 30 years to do it, longer than any of these scientist's careers, which means basically never)
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q30" value="option2">
                                            <label for="q30">
                                                In 50 years
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q31" value="option3">
                                            <label for="q31">
                                                Maybe Never like many reputable scientists at California Livermore laboratory claim
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q32" value="option4">
                                            <label for="q32">
                                                Never because fusion takes energy to do an is a net loss function into a stable element and immediately stops excess radiation at any more than
                                            </label>
                                            <label for="q32">&nbsp;&nbsp;&nbsp;&nbsp;normal level once fused.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>California's economy grew by how much after they increased their renewable energy promotion policies and environmental standards?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question7" id="q33" value="option1">
                                            <label for="q33">
                                                5%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question7" id="q34" value="option2">
                                            <label for="q34">
                                                10%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question7" id="q35" value="option3">
                                            <label for="q35">
                                                12%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question7" id="q36" value="option4">
                                            <label for="q36">
                                                More than 14%
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="general-input gold-font">
                                    <label for="InputName">Your Name</label>
                                    <input type="text" class="form-control" id="InputName" placeholder="Your Name">
                                </div>
                                <br/>
                                <div class="email-form gold-font">
                                    <label for="InputEmail">Email address</label>
                                    <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Enter your email">
                                    <small id="emailHelp" class="form-text">We'll never share your email with anyone else.</small>
                                </div>
                                <div class="submit-button">
                                    <input type="submit" class="btn btn-primary"></input>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
                <div class="col-1 col-sm-1 col-md-1"></div>
            </div>
        </div>
    </div>

<?php include("footer.php"); ?>