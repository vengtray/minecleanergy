<?php include "header.php";?>

    <div id="video-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="col-12 col-md-12 col-sm-12">
                            <div class="title">
                                <h1>VIDEOS</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-12 col-md-4 col-sm-4">
                            <video controls>
                                <source src="video/video.MP4" type="video/mp4">
                            </video>
                            <div class="description white-font center">
                                <h3>The All Out War</h3>
                                <p class="gold-font">AOE Declares War on the Naysayers and Blockers of Energy Innovation and Development Power,
                                    and Charges Ahead With All New Breakthrough after Breakthrough</p>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 col-sm-4">
                            <video controls>
                                <source src="video/AOE_Cambo flashy_video_on Vimeo.MP4" type="video/mp4">
                            </video>
                            <div class="description white-font center">
                                <h3>From Ground Zero to The Charge of History, Welcome to the Age of Accelleration</h3>
                                <p class="gold-font">It's time to begin the new Era, support the change the world charge</p>

                            </div>
                        </div>
                        <div class="col-12 col-md-4 col-sm-4">
                            <video controls>
                                <source src="video/Alpha_Omega_Energy _Goes_ICO.MP4#t=00:00:01" type="video/mp4">
                            </video>
                            <div class="description white-font center">
                                <h3>CHARGE IT ALL</h3>
                                <p class="gold-font">AOE Exhibits at Echelon Asia and SWITCH Singapore Week of Innovation and Technology, and Takes on the Energy Battle for all of Humanity</p>

                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-12 col-md-4 col-sm-4">
                            <video controls>
                                <source src="video/invest.mp4" type="video/mp4">
                            </video>
                            <div class="description white-font center">
                                <h3>The Massive Need To Invest in AOE</h3>

                            </div>
                        </div>
                        <div class="col-12 col-md-4 col-sm-4">
                            <iframe src="https://player.vimeo.com/video/273884844" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
                            <p><a href="#" style="color: white; font-size: 28px;">Alpha Omega Energy and AOECOIN. The World&#039;s #1 Breakthrough CleanTech Startup.</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include "footer.php";?>