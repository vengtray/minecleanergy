<?php include("header.php"); ?>

    <!-- Content -->
    <div id="mine-ergy">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="title gold-font">
                        <h3>MineCleanergy DATACENTER</h3>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-1 col-sm-1 col-md-1"></div>
                <div class="col-10 col-sm-1 col-md-10">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <p>MineCleanergy Datacenter is a modern, hi-tech facility, designed to house and host cryptoequipment.</p>
                                <p>Facility’s temperature and humidity is kept at constant values according to industry standards. This
                                    allows for mining at maximum hashrates without overheating the equipment.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                GENERAL
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            Buying MineCleanergy of equipment hosted in the MineCleanergy datacenter you rest assured that the mining equipment is placed,
                                            set up and maintained by a team of qualified personnel.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                FACILITY
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">
                                            The datacenter is built by a first-class industrial construction company with dozens of years of experience in such projects.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                SECURITY AND COMPLIANCE
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            Security is a critical factor of a datacenter. We have ensured compliance with the highest international standards with sophisticated
                                            manned, hardware and software security systems.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFour">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                                ELECTRICITY
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                        <div class="card-body">
                                            Power is provided by independent power grids with a separate generator and a backup batteries.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFive">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                                                CONNECTION
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                        <div class="card-body">
                                            Internet connection is provided by redundant fiber connections to different Estonian internet exchanges.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingSix">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
                                                TECHNICAL SUPPORT
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                                        <div class="card-body">
                                            24/7 maintenance provided by professional on-site support to effectively resolve any technical issues.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingSeven">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseThree">
                                                ADVANTAGES
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                                        <div class="card-body">
                                            <ul>
                                                <li>Scalability and a choice of mining plans</li>
                                                <li>Carrier neutral, redundant connections</li>
                                                <li>Secure facility and flexibility</li>
                                                <li>Designed for cryptocurrency mining equipment</li>
                                                <li>ISKE standard compliance</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h3>TECHICAL SPECIFICATIONS</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingEight">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseEight" aria-expanded="true" aria-controls="collapseOne">
                                                DATACENTER FEATURES
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                                        <div class="card-body">
                                            <ul>
                                                <li>Mining Equipment Hosting</li>
                                                <li>Racks up to 8kW</li>
                                                <li>Internet Connectivity</li>
                                                <li>Remote Administration Racks</li>
                                                <li>APC / Rittal</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingNine">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseTwo">
                                                GENERAL FEATURES
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                                        <div class="card-body">
                                            <ul>
                                                <li>
                                                    <b>Structure</b>: Reinforced concrete</li>
                                                <li>
                                                    <b>Floors</b>: Ground and raised</li>
                                                <li>
                                                    <b>Area</b>: Low flood area. DC rooms 20m above sealevel</li>
                                                <li>
                                                    <b>On-site personnel</b>: Electrical engineers, Network engineers, Security personnel,
                                                    Monitoring staff</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTen">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseThree">
                                                STANDARDS COMPLIANCE
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                                        <div class="card-body">
                                            <ul>
                                                <li>
                                                    <b>Data Centre</b>: ISKE standard compliant rooms, TIER level 2</li>
                                                <li>
                                                    <b>Certifications</b>: EVS-EN ISO 9001:2008 Certificate for Quality Management System</li>
                                                <li>
                                                    <b>Qualifications</b>: ITIL qualified personnel</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingEleven">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseThree">
                                                CONNECTIVITY
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordion">
                                        <div class="card-body">
                                            <ul>
                                                <li>
                                                    <b>Features</b>: 2 independent optic fiber routes</li>
                                                <li>
                                                    <b>Operators</b>: TeliaSonera Elion</li>
                                                <li>
                                                    <b>Technologies</b>: SDH, MPLSl</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwelve">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwelve" aria-expanded="false" aria-controls="collapseThree">
                                                POWER
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve" data-parent="#accordion">
                                        <div class="card-body">
                                            <ul>
                                                <li>
                                                    <b>External feed</b>: 4 independent connections to the national power grid</li>
                                                <li>
                                                    <b>Power</b>: 2MW</li>
                                                <li>
                                                    <b>Voltage</b>: 4x 10kV</li>
                                                <li>
                                                    <b>Sub-station</b>: Automatic Transfer Switch (ATS)</li>
                                                <li>
                                                    <b>UPS systems</b>: N+1 redundancy configuration</li>
                                                <li>
                                                    <b>Generators</b>: Diesel, 850kVA in N+1 configuration. Separately housed</li>
                                                <li>
                                                    <b>Fuel supply</b>: Fuel for 48 hours of continual service at site</li>
                                                <li>
                                                    <b>Cabling</b>: Waterproof and fire resistant cabling on 3 level trays</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThirteen">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThirteen" aria-expanded="false" aria-controls="collapseThree">
                                                COOLING
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen" data-parent="#accordion">
                                        <div class="card-body">
                                            <ul>
                                                <li>
                                                    <b>Layout</b>: Hot and cold aisle layout</li>
                                                <li>
                                                    <b>Features</b>: Traditional and free air cooling with heat exchangers and air conditioners</li>
                                                <li>
                                                    <b>Redundancy</b>: Air conditioners in N+1 redundancy</li>
                                                <li>
                                                    <b>Temperature</b>: 20-24°C – According to industry standards</li>
                                                <li>
                                                    <b>Humidity</b>: According to industry standards</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFourteen">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourteen" aria-expanded="false" aria-controls="collapseThree">
                                                SAFETY & PROTECTION
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFourteen" class="collapse" aria-labelledby="headingFourteen" data-parent="#accordion">
                                        <div class="card-body">
                                            <ul>
                                                <li>
                                                    <b>Fire</b>: Gas extinguishing fire suppression system</li>
                                                <li>
                                                    <b>Water</b>: Drainage systems with pumps in N+1 redundancy</li>
                                                <li>
                                                    <b>Intrusion</b>: Detection with sensors, video and on-site personnel</li>
                                                <li>
                                                    <b>Surveillance</b>: 24h on-site manned surveillance, Video surveillance</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-1 col-sm-1 col-md-1"></div>
            </div>
        </div>
    </div>
    <!-- End -->

<?php include("footer.php"); ?>