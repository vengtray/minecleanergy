<?php include("header.php"); ?>

<div id="investment">
    <div class="container">
        <div class="row">
            <div class="col-1 col-sm-1 col-md-1"></div>
            <div class="col-10 col-sm-10 col-md-10">
                <div class="title">
                    <h1>USA INVESTS MORE MONEY INTO ASEAN REGION THAN ANY OTHER EVEN THAN CHINA, JAPAN, AND AUSTRALIA</h1>
                </div>
                <div class="row">
                    <div class="col-6 col-md-6 col-sm-6">
                        <div class="pictures">
                            <img src="img/investment.png" alt="investment">
                        </div>
                    </div>
                    <div class="col-6 col-md-6 col-sm-6">
                        <div class="pictures">
                            <img src="img/leadership.png" alt="leadership">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="title" style="margin-top: 100px;">
                        <h3>CAMBODIA’S ECONOMY - VIDEOS - GET EDUCATED</h3>
                    </div>
                    <div class="text">
                        <p>Click Here to Watch for Yourself and Become Educated on Cambodia's great Investment potential, the economy, development zones, life in Cambodia, Real Estate development, and the coming Incredible Success of your Investment and Support Of Alpha Omega Energy. Videos from Bloomberg, CNBC, CNN, iProfile, and many more!</p>
                    </div>
                    <div class="link">
                        <a href="https://aoecoin.io/cambodia-videos" class="btn btn-primary">Get More</a>
                    </div>
                </div>
            </div>
            <div class="col-1 col-sm-1 col-md-1"></div>
        </div>
    </div>
</div>


<?php include("footer.php"); ?>