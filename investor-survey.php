<?php include("header.php"); ?>

    <div id="investor-survey">
        <div class="container">
            <div class="row">
                <div class="col-1 col-sm-1 col-md-1"></div>
                <div class="col-10 col-sm-10 col-md-10">
                    <div class="logo-img">
                        <img src="img/aoelogo.png" alt="alpha">
                    </div>
                    <div class="line"></div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h1>Invest in AOECoin?</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="text gold-font">
                                <p class="center">We would love to hear your thoughts in our survey! Help Us Help You!</p>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <form action="#" method="POST">
                                <div class="radio-form gold-font">
                                    <h3>Will you Invest and Support the Change of the Energy Future</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question" id="q1" value="option1">
                                            <label for="q1">
                                                Wow this is awesome I am in all the way
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question" id="q2" value="option2">
                                            <label for="q2">
                                                I don't have money to invest but I will help the campaign and find others to invest
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question" id="q3" value="option3">
                                            <label for="q3">
                                                I will volunteer to support
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question" id="q4" value="option4">
                                            <label for="q4">
                                                10 million dead a year just from breathing? "Let them all die people have been dying for thousands of years! 
                                            </label>
                                            <label for="q4">&nbsp;&nbsp;&nbsp;&nbsp;They will die anyway!" (Quote by J Menzies, Gas company Executive)</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How much do you plan on investing into AOECOIN?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q5" value="option1">
                                            <label for="q5">
                                                $0-100
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q6" value="option2">
                                            <label for="q6">
                                                $101-500
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q7" value="option3">
                                            <label for="q7">
                                                $500-2000
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q8" value="option4">
                                            <label for="q8">
                                                $2,001-5000
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q9" value="option5">
                                            <label for="q9">
                                                $5,001-10,000
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q10" value="option6">
                                            <label for="q10">
                                                $10,001-25,000
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q11" value="option7">
                                            <label for="q11">
                                                $25,001-50,000
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q12" value="option8">
                                            <label for="q12">
                                                $50,001-99,999
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q13" value="option9">
                                            <label for="q13">
                                                Option 100,000+
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q14" value="option10">
                                            <label for="q14">
                                                Option 10
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="checkbox-form gold-font">
                                    <h3>Are you looking for a long term investment with stable high growth (10% plus) that has asset backing, some regular income to reinvest, or are you more of a flip daytrader?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q15" value="option1">
                                            <label for="q15">
                                                Flippy flip all the way, hopefully I don''t file bankruptcy one day!    
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q16" value="option2">
                                            <label for="q16">
                                                I'm all about long term warren buffet style sleep at night never look for 10 years and it will still be there investments like AOECoin
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q17" value="option3">
                                            <label for="q17">
                                                I like buy and hold but I am lured by the greed side of crypto too much to invest in AOECoin
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q18" value="option4">
                                            <label for="q18">
                                                Love it, and will invest heavily
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How interested are you in telling your friends and others about AOECoin?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q19" value="option1">
                                            <label for="q19">
                                                Im telling everyone!! This is awesome!
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q20" value="option2">
                                            <label for="q20">
                                                I think ill tell a few close conifdantes
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q21" value="option3">
                                            <label for="q21">
                                                I'm not so comfortable about recommending things to people
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>If we give you an extra 100% Bonus coin, how likely would you be to sell your house and invest in AOECOIN and help us shove their toxic BS down their God damn throats? (AOE open to this offer)</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q22" value="option1">
                                            <label for="q22">
                                                IM IN!!!! SOLD, calling the realtor right now.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q23" value="option2">
                                            <label for="q23">
                                                I’ll take a good look and consider it, we need this.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q24" value="option3">
                                            <label for="q24">
                                                DUDE!! Put me on the list, FIRST! No really! I want this coin!!!
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q24" value="option3">
                                            <label for="q25">
                                                Pass, a world full of completely toxic, polluted, destroyed lakes, rivers, water tables, toxified food, nuclear waste piles, 
                                            </label>
                                            <label for="q25">
                                                &nbsp;&nbsp;&nbsp;endless cancers, hundreds of millions dead, 10 meter sea level rises, leaking reactors into the ocean destroying the entire world,and a lights out
                                            </label>
                                            <label for="q25">&nbsp;&nbsp;&nbsp;planet of iniquity, lack of development, pollution, insanity, and muslim oil terror financing is just so much better than basic f-ing sense.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>Energy is 1/5th of the global economy. Therefore how much of my money should I be investing to solve this massive global corruption problem?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q26" value="option1">
                                            <label for="q26">
                                                1/5th
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q27" value="option2">
                                            <label for="q27">
                                                1/5th of my savings and investment dollars
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q28" value="option3">
                                            <label for="q28">
                                                All of it since what it will do for the world is worth many more times than that
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q29" value="option4">
                                            <label for="q29">
                                                None, my IQ, EQ, and care for humanity is pinging a pace like a snail on a race.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>If won't invest or support, why not?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q30" value="option1">
                                            <label for="q30">
                                                I don't believe in technology
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q31" value="option2">
                                            <label for="q31">
                                                I'm new to blockchain
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q32" value="option3">
                                            <label for="q32">
                                                I suck at choosing investments
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q33" value="option4">
                                            <label for="q33">
                                                What the heck is a bitcoin?
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q34" value="option5">
                                            <label for="q34">
                                                I'm a strong believer in worshipping the toxic-energy-scicom religion and that no solution can ever be made to solve anything.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="submit-button">
                                    <input type="submit" class="btn btn-primary"></input>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-1 col-sm-1 col-md-1"></div>
            </div>
        </div>
    </div>

<?php include("footer.php"); ?>