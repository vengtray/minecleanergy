<?php include("header.php"); ?>

    <div id="leadership">
        <div class="container">
            <div class="row">
                <div class="col-1 col-sm-1 col-md-1"></div>
                <div class="col-10 col-sm-10 col-md-10">
                    <div class="logo-img">
                        <img src="img/aoelogo.png" alt="alpha">
                    </div>
                    <div class="line"></div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font">
                                <h1>Economic Leadership Quiz</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="text gold-font">
                                <p class="center">We would love to hear your thoughts or feedback on how we can improve your experience!</p>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <form action="#" method="POST">
                                <div class="radio-form gold-font">
                                    <h3>99% of New Jobs are Created in the Small Business and SME Sector, and only 1% in the Large corporation and Government sectors. How much % age of subsidy money handouts does the proven success of the Small Businesses get?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question" id="q1" value="option1">
                                            <label for="q1">
                                                90%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question" id="q2" value="option2">
                                            <label for="q2">
                                                50%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question" id="q3" value="option3">
                                            <label for="q3">
                                                80%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question" id="q4" value="option4">
                                            <label for="q4">
                                                Less than 1%
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>What is the renewable energy penetration in Southeast Asia, a region known for toxic energy corruption in nearly every nation?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q5" value="option1">
                                            <label for="q5">
                                                10%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q6" value="option2">
                                            <label for="q6">
                                                5%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q7" value="option3">
                                            <label for="q7">
                                                3%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question1" id="q8" value="option4">
                                            <label for="q8">
                                                Less than 1%
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="checkbox-form gold-font">
                                    <h3>How much funding is there available to citizens who want to start a new renewable energy business in Southeast Asia?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q9" value="option1">
                                            <label for="q9">
                                                All of it since power and clean energy is desperately needed to improve economic development in the region
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q10" value="option2">
                                            <label for="q10">
                                                50% of the money is immediately available to those who wan to begin this kind of business
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q11" value="option3">
                                            <label for="q11">
                                                10% is available with large restrictions on operation and what equipment you buy and from where
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="checkbox" name="checkbox-question" id="q12" value="option4">
                                            <label for="q12">
                                                Absolutely squat there is zero funding from anywhere to start a business in this sector.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>Do you believe in the so called "Clean Coal" snake oil salesman story</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q13" value="option1">
                                            <label for="q13">
                                                Rediculous BS! There is no such thing as clean coal. Coal is burned and CO2 is just pumped into some rock and
                                            </label>
                                            <label for="q13">&nbsp;&nbsp;&nbsp;&nbsp;it will always leak into everything else over time eventually</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q14" value="option2">
                                            <label for="q14">
                                                Real renewables are a far better solution and stopping the digging of coal in the first place is far better than the pollutions it leaves behind
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q15" value="option3">
                                            <label for="q15">
                                                So called clean coal is just a scam so the coal industry can keep going, just like the fusion scam
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question2" id="q16" value="option4">
                                            <label for="q16">
                                            I believe that miraculously beyond physics that the CO2 won't actually escape the rocks its pumped into and that it will magically stay in there
                                            </label>
                                            <label for="q16">&nbsp;&nbsp;&nbsp;&nbsp;despite mixing with groundwater and that it's a gaseous state which will act not much different than Co2 in a coke cup. It won't</label>
                                            <label for="q16">&nbsp;&nbsp;&nbsp;&nbsp;get out of there mate She'll be roight mate!!!.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>What do you think about stored nuclear waste?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q17" value="option1">
                                            <label for="q17">
                                                The nuclear industry and governments are lying to everyone as to not create mass panic and that in 40-100 years all the containment barrels
                                            </label>
                                            <label for="q17">&nbsp;&nbsp;&nbsp;&nbsp;and systems will erode and fail and the whole planet will be radiated to point of no return</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q18" value="option2">
                                            <label for="q18">
                                                For 100+ years we have already has technology that can make electricity from nuclear waste and if the Small Private innovators
                                            </label>
                                            <label for="q18">&nbsp;&nbsp;&nbsp;&nbsp;are given funding they will solve this problem quickly</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q19" value="option3">
                                            <label for="q19">
                                                The containment systems are only made so that toxic nuclear industry can get massive contracts to run these facilities
                                            </label>
                                            <label for="q19">&nbsp;&nbsp;&nbsp;&nbsp;instead of forcing energy prices down by using it to make electricity many times.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question3" id="q20" value="option4">
                                            <label for="q20">
                                                We don't need nuclear at all and should ban nuclear plants totally to force renewables production and economic growth through these jobs.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>Some say that Government officials and huge clean energy corporations have destroyed innovation in new energy due to their Greed.</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q21" value="option1">
                                            <label for="q21">
                                                It's definitely the case because if this wasn't so, then you would hear continually about new fundings to Small Private Innovators
                                            </label>
                                            <label for="q21">&nbsp;&nbsp;&nbsp;&nbsp;and it just never happens.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q22" value="option2">
                                            <label for="q22">
                                                Everyone knows that 99% of VC doesn't fund innovation stage CleanTech and those that do, in 90% of cases only fund in their own countries,
                                            </label>
                                            <label for="q22">&nbsp;&nbsp;&nbsp;&nbsp;leaving global innovation funding at this crucial innovation stage dead and unable to gain support. Officials and energy corps also of course</label>
                                            <label for="q22">&nbsp;&nbsp;&nbsp;&nbsp;never fund the Small Private Innovators with Breakthrough techs. So this is a yes for me.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question4" id="q23" value="option3">
                                            <label for="q23">
                                                This is capitalism mate, they have a right to be greedy! and no one should do anything about it, just like in China
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How much effort has been made by climate activists, ad sales networks (media) and lobbyists and climate researchers, to find, fund, and prop up and support the Small Private Cleantech Breakthrough innovators?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q24" value="option1">
                                            <label for="q24">
                                                None practically, especially considering what should be done. A near blank out zero.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q25" value="option2">
                                            <label for="q25">
                                                I have never heard of a single one before that they have announced and supported in my life come to think of it.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q26" value="option3">
                                            <label for="q26">
                                                No more than 10% of the amount of support they should have given
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question5" id="q27" value="option4">
                                            <label for="q27">
                                                The ONLY reason we have these problems in the first place is because these guys haven't done this crucial and minimum mandatory
                                            </label>
                                            <label for="q27">&nbsp;&nbsp;&nbsp;&nbsp;step of competance to even claim they care about climate change, pollution, or economic development.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How fast could government and these Cop-23 crowds of tens of thousands of government officers and executives prop up solutions if they actually wanted to?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q28" value="option1">
                                            <label for="q28">
                                                If they actually wanted to they could fund this just like any other major infrastructure project and solve it virtually overnight
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q29" value="option2">
                                            <label for="q29">
                                                It would still take years, unless they invested into the Small Private Innovators network directly and heavily
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q30" value="option3">
                                            <label for="q30">
                                                Using the internet, they could Instantly find AOE and call them up and fund them directly enough for a trial with contract of scale funding
                                            </label>
                                            <label for="q30">&nbsp;&nbsp;&nbsp;&nbsp;post result. One day is all it should have taken them even decades ago and they all know this too but refuse to do it.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question6" id="q31" value="option4">
                                            <label for="q31">
                                                They don't want to give funding to anything not huge business late stage established already, they prefer their corruption business games
                                            </label>
                                            <label for="q31">&nbsp;&nbsp;&nbsp;&nbsp;instead and will never fund the SPI market participants, never have never will, but could in short order</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How many solar panels are on the World bank's offices, and should they be declared incompetant and morally defunct and in need of a total overhaul if there are not enough as 10 million a year are dead just from breathing?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question7" id="q32" value="option1">
                                            <label for="q32">
                                                I think 100% of World bank offices are covered to the max, It seems insane not to have it like this.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question7" id="q33" value="option2">
                                            <label for="q33">
                                                90% I would guess it's 90% at least they would have done by now, surely it would be a huge priority and requirement to be called
                                            </label>
                                            <label for="q33">&nbsp;&nbsp;&nbsp;&nbsp;a competent financial person</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question7" id="q34" value="option3">
                                            <label for="q34">
                                                50-50
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question7" id="q35" value="option4">
                                            <label for="q35">
                                                They don't have a single solar panel on many if not most of their offices, thus are really completely incompetant hypocrites
                                            </label>
                                            <label for="q35">&nbsp;&nbsp;&nbsp;&nbsp;who are directly killing more people due to their unwillingness to do anything about it but rather people be dead.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How much money does World Bank fund or lend to SMEs and Startups solving the clean energy problem?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question8" id="q36" value="option1">
                                            <label for="q36">
                                                500 billion a year
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question8" id="q37" value="option2">
                                            <label for="q37">
                                                1 billion a year
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question8" id="q38" value="option3">
                                            <label for="q38">
                                                5 million a year
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question8" id="q39" value="option4">
                                            <label for="q39">
                                                Absolutely jack, zero, zilch, nada, zero, showing they couldn't care less about it and the 10 million dead every year due to, showing their
                                            </label>
                                            <label for="q39">&nbsp;&nbsp;&nbsp;&nbsp;total incompetance to solving this problem and total incompetance in assisting this economy which creates 99% of the jobs in USA renewables.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How much do climate research Orgs, enviro lobbyists and campaigners fund to the SPI CleanTech market?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question9" id="q40" value="option1">
                                            <label for="q40">
                                                Nothing
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question9" id="q41" value="option2">
                                            <label for="q41">
                                                Not enough thats for sure
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question9" id="q42" value="option3">
                                            <label for="q42">
                                                They should have to by law fund at least 50% of all money that comes in the door.
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question9" id="q43" value="option4">
                                            <label for="q43">
                                                There are certainly no programs for the SPIs from this sector, but they don't need funding because they can do the Impossible anyway
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How much of Bill Gate's BEV (breakthrough energy ventures) fund goes to SPI’s?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question10" id="q44" value="option1">
                                            <label for="q44">
                                                90%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question10" id="q45" value="option2">
                                            <label for="q45">
                                                80%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question10" id="q46" value="option3">
                                            <label for="q46">
                                                50%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question10" id="q47" value="option4">
                                            <label for="q47">
                                                Absolutely none. In fact, he stated on his website that if anyone sends in their technologies that they will be stolen, and that they are not
                                            </label>
                                            <label for="q47">&nbsp;&nbsp;&nbsp;&nbsp;the proper people to fund to solve this global problem, only the crony insanely wasteful government-university establishment is the proper</label>
                                            <label for="q47">&nbsp;&nbsp;&nbsp;&nbsp;place.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How much of the 400 billion a year research money the USA spends actually goes to the private market?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question11" id="q48" value="option1">
                                            <label for="q48">
                                                50%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question11" id="q49" value="option2">
                                            <label for="q49">
                                                20%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question11" id="q50" value="option3">
                                            <label for="q50">
                                                5%
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question11" id="q51" value="option4">
                                            <label for="q51">
                                                less than 3%, and much much less than 0.5% to the Small Private innovator's market.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How many SPIs are fully funded to go to COP 23 each year?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question12" id="q52" value="option1">
                                            <label for="q52">
                                                None
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question12" id="q53" value="option2">
                                            <label for="q53">
                                                1
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question12" id="q54" value="option3">
                                            <label for="q54">
                                                2
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question12" id="q55" value="option4">
                                            <label for="q55">
                                                3
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How many nations have an open grid policy? Meaning someone builds a brand new technology and then feeds in their power into the grid without any restrictions and zero IP access by anyone, keeping their IP privacy, and gets paid by the government for whatever power they put in, regardless of the technology used?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question13" id="q52" value="option1">
                                            <label for="q52">
                                                No grid is open in the whole entire world
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question13" id="q53" value="option2">
                                            <label for="q53">
                                                Officials TALK as if they want a solution but the required MINIMUM standard is open grids, and it isn't happening, and that is a 100% litmus
                                            </label>
                                            <label for="q53">&nbsp;&nbsp;&nbsp;&nbsp;test of government and energy department corruption, prosecutable accordingly and instantly. Proof done.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question13" id="q54" value="option3">
                                            <label for="q54">
                                                Nearly all grids are open
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question13" id="q55" value="option4">
                                            <label for="q55">
                                                They are only open for solar and wind and gas, but the existing establishments must first scan through everything in all the equipment to steal
                                            </label>
                                            <label for="q55">&nbsp;&nbsp;&nbsp;&nbsp;and check out the IP first before they will even consider allowing it, then give it a destructive pricing model that limits the reward based on the</label>
                                            <label for="q55">&nbsp;&nbsp;&nbsp;&nbsp;profit amount, thus killing dead the competition and speed of change, which is monopolistic anti-trust corruption in business practices illegal in</label>
                                            <label for="q55">&nbsp;&nbsp;&nbsp;&nbsp;nearly every other industry.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>The biggest solar conference in Southeast Asia called "Unlocking Solar Capital Asia" had what kind of programs, policies, markets and new markets and incentives for Unlocking capital and making finance available for startups to innovators to SMEs to then grow and promote and foster grassroots economic growth in Clean Energy and Innovation?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question14" id="q56" value="option1">
                                            <label for="q56">
                                                The mega corporation speakers stood on stage and literally said to government "it's all about the money." And the event promoted nothing
                                            </label>
                                            <label for="q56">&nbsp;&nbsp;&nbsp;&nbsp;to do with early stage development, completely ignoring innovation and new participants completely.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question14" id="q57" value="option2">
                                            <label for="q57">
                                                The massive lenders sat on stage in a panel whining that projects arent bankable, then refused to give out lending guidelines at all so that
                                            </label>
                                            <label for="q57">&nbsp;&nbsp;&nbsp;&nbsp;project developers could design their projects to the guidelines thus make them bankable every single time, then the lenders and economic</label>
                                            <label for="q57">&nbsp;&nbsp;&nbsp;&nbsp;development boards whined how Southeast asia has only not even 1% of solar penetration and asked the megacorps not the grassrooters</label>
                                            <label for="q57">&nbsp;&nbsp;&nbsp;&nbsp;nor disruptors how to change it, and were answered with "Give us more money."</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question14" id="q58" value="option3">
                                            <label for="q58">
                                                Unimaginably, all of the *^*@*#Y above. Talk about total and unimagnable incompetance and the biggest useless waste of time in energy
                                            </label>
                                            <label for="q58">&nbsp;&nbsp;&nbsp;&nbsp;industry history.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question14" id="q59" value="option4">
                                            <label for="q59">
                                                Zilch, absolutely nothing. Not a single loan program was promoted, not a single set of guidelines issued nor given, not a single lender was
                                            </label>
                                            <label for="q59">&nbsp;&nbsp;&nbsp;&nbsp;offering anything different whatsofreakingever.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How many Startups and SME in this sector does the IFC fund? And how many should they to what effect?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question15" id="q60" value="option1">
                                            <label for="q60">
                                                What? They don't fund any of them, of course they don't, so the problem goes on, forget leadership! Aint never gonna happen!
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question15" id="q61" value="option2">
                                            <label for="q61">
                                                IFC has a global leadership responsibility aspect in this regard and has failed completely in providing any kind of leadership solutions for
                                            </label>
                                            <label for="q61">&nbsp;&nbsp;&nbsp;&nbsp;this whatsoever. They should be hunting down companies like AOE and funding the hell out of them to force commercializations not waiting</label>
                                            <label for="q61">&nbsp;&nbsp;&nbsp;&nbsp;for 10 years until they are a megacap corporation and lying on stages as they do that they supposedly support SME when hey don't touch a</label>
                                            <label for="q61">&nbsp;&nbsp;&nbsp;&nbsp;single SME but to give massive SME BANKS money so they can loan it on in prohibitive financings based on collateral only.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question15" id="q62" value="option3">
                                            <label for="q62">
                                                I never looked into it, I just assumed they funded them or would and that they had the competence to work on these major issues,
                                            </label>
                                            <label for="q62">&nbsp;&nbsp;&nbsp;&nbsp;Wow was I fooled!</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How many Small Private Energy Innovators did famous showboater master of lying with hockeystick charts Al Gore invest in, support, and promote that other people invest in, on his global tours promoting and glorifying himself for huge speaking fees?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question16" id="q63" value="option1">
                                            <label for="q63">
                                                None
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question16" id="q64" value="option2">
                                            <label for="q64">
                                                Zero, not ever, you can't find one, and no one in his movies
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question16" id="q65" value="option3">
                                            <label for="q65">
                                                He promoted that he was the inventor of the internet and was abducted by aliens and told he was the ambassador for all humanity,
                                            </label>
                                            <label for="q65">&nbsp;&nbsp;&nbsp;&nbsp;so he doesn't need to promote any innovation.</label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question16" id="q66" value="option4">
                                            <label for="q66">
                                                Zilch, talk only, Zilch Zero Zilch
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="radio-form gold-font">
                                    <h3>How many Breakthrough Technology Innovators have died in the last 20 years which NO ONE in the "Clean Energy industry" and especially no governments, are then funding the Small Private Innovators who dug up their tech to use and commercialize to solve this problem and create millions of jobs?</h3>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question17" id="q67" value="option1">
                                            <label for="q67">
                                                10
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question17" id="q68" value="option2">
                                            <label for="q68">
                                                20
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question17" id="q69" value="option3">
                                            <label for="q69">
                                                30
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <input type="radio" name="radio-question17" id="q70" value="option4">
                                            <label for="q70">
                                                50+
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="general-input gold-font">
                                    <label for="InputName">Your Name</label>
                                    <input type="text" class="form-control" id="InputName" placeholder="Your Name">
                                </div>
                                <br/>
                                <div class="email-form gold-font">
                                    <label for="InputEmail">Email address</label>
                                    <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Enter your email">
                                    <small id="emailHelp" class="form-text">We'll never share your email with anyone else.</small>
                                </div>
                                <div class="submit-button">
                                    <input type="submit" class="btn btn-primary"></input>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-1 col-sm-1 col-md-1"></div>
            </div>
        </div>
    </div>

<?php include("footer.php"); ?>