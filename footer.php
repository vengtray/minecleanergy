<!-- Footer -->

    <div id="language">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="switch-lang center">
                    <a href="#">English | </a>
                    <a href="#">Japanese | </a>
                    <a href="#">Khmer</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Social -->
    <div id="social-link">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="title gold-font center">
                    <h1>Join Us Now</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="social gold-font center">
                            <a href="mailto:ctw@aomegaenergy.com">
                                <i class="fas fa-envelope fa-3x"></i>
                                <span>Email us or Chat to Investor Services Now!</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="social gold-font center">
                            <a href="https://t.me/AOECOINCHANNEL" target="_blank">
                                <i class="fab fa-telegram fa-3x"></i>
                                <span>Sign up for the live updates on Telegram!</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="social gold-font center">
                            <a href="https://t.me/AOECOIN" target="_blank">
                                <i class="fab fa-telegram fa-3x"></i>
                                <span>Sign up for the live Telegram Chat group!</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="footer-img">
                            <a href="https://www.linkedin.com/in/aomegaenergy" target="_blank">
                                <img src="img/Linkin-Aoe-01.png">
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="steem gold-font center">
                            <a href="https://steemit.com/@alphaomegaenergy" target="_blank">STEEM
                                <span>BLOG</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="medium">
                            <a href="https://medium.com/@aomegaenergy" target="_blank">
                                <img src="img/medium.png">
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="twitter">
                            <a href="https://twitter.com/AOECOIN" target="_blank">
                                <img src="img/twitter.png">
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="linken">
                            <a href="https://www.linkedin.com/in/aomegaenergy" target="_blank">
                                <img src="img/linken.png">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Link-Footer -->
    <div class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-2  col-md-2 col-sm-4 col-xs-6">
                    <h3> Our Offerings </h3>
                    <ul>
                        <li>
                            <a href="#"> Electricity </a>
                        </li>
                        <li>
                            <a href="#"> Solar </a>
                        </li>
                        <li>
                            <a href="#"> Cloud Mining </a>
                        </li>
                        <li>
                            <a href="#"> Finance </a>
                        </li>
                        <li>
                            <a href="http://aomegaenergy.tilda.ws/tech" target="_blank"> Technology </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-2  col-md-2 col-sm-4 col-xs-6">
                    <h3> Company </h3>
                    <ul>
                        <li>
                            <a href="http://aomegaenergy.tilda.ws/aboutus" target="_blank"> About Us </a>
                        </li>
                        <li>
                            <a href="contact.php"> Contact Us </a>
                        </li>
                        <li>
                            <a href="#"> News & Events </a>
                        </li>
                        <li>
                            <a href="#"> Careers </a>
                        </li>
                        <li>
                            <a href="#"> Privacy Policy </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-2  col-md-2 col-sm-4 col-xs-6">
                    <h3> Markets </h3>
                    <ul>
                        <li>
                            <a href="https://aoecoin.io/cambodia" target="_blank"> Cambodia </a>
                        </li>
                        <li>
                            <a href="#"> Global Expansion </a>
                        </li>
                        <li>
                            <a href="#"> Renewable Energy </a>
                        </li>
                        <li>
                            <a href="#"> Finance </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-2  col-md-2 col-sm-4 col-xs-6">
                    <h3> Value-Added Service </h3>
                    <ul>
                        <li>
                            <a href="#"> Energy Audits </a>
                        </li>
                        <li>
                            <a href="#"> Consultation </a>
                        </li>
                        <li>
                            <a href="#"> Solar Repair </a>
                        </li>
                        <li>
                            <a href="http://aomegaenergy.tilda.ws/invest" target="_blank"> Investments </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3  col-md-3 col-sm-6 col-xs-12 ">
                    <h3> Resources </h3>
                    <ul>
                        <li>
                            <a href="http://aomegaenergy.tilda.ws/" target="_blank"> Aomegaenergy.com </a>
                        </li>
                        <li>
                            <a href="https://www.zacks.com/" target="_blank"> Zacks.com </a>
                        </li>
                        <li>
                            <a href="https://www.bloomberg.com/asia" target="_blank"> Bloomberg.com </a>
                        </li>
                        <li>
                            <a href="http://www.csx.com.kh/main.do?lang=kh" target="_blank"> Csx.com.kh </a>
                        </li>
                        <li>
                            <a href="https://www.biblegateway.com/" target="_blank"> Biblegateway.com </a>
                        </li>
                        <li>
                            <a href="http://aomegaenergy.tilda.ws/backgroundradiation" target="_blank"> Macrowave
                                <br/> &nbsp; background
                                <br/> &nbsp; radiation </a>
                        </li>
                    </ul>
                </div>
            </div>
            <button onclick="topFunction()" id="myBtn" title="Go to top">
                <i class="fas fa-angle-up"></i>
            </button>
        </div>
    </div>
    <br/>
    <!-- Fixed Footer -->
    <div id="fixed-footer">
        <div class="row">
            <div class="col-3 col-sm-3 col-md-3">
                <div class="btn-group dropup">
                    <a href="#" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        CONTACT SALES
                    </a>
                    <div class="dropdown-menu">
                        <a href="mailto:ctw@aomegaenergy.com">Email</a>
                        <a href="https://t.me/AOECOINCHANNEL">Telegram Channel</a>
                    </div>
                </div>
            </div>
            <div class="col-3 col-sm-3 col-md-3">
                <div class="btn-group dropup">
                    <a href="order.php">ORDER</a>
                </div>
            </div>
            <div class="col-3 col-sm-3 col-md-3">
                <div class="btn-group dropup">
                    <a href="suggestion.php">SUGGESTION</a>
                </div>
            </div>
            <div class="col-3 col-sm-3 col-md-3">
                <div class="btn-group dropup">
                    <a href="#" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        SURVEY
                    </a>
                    <div class="dropdown-menu">
                        <a href="climate.php">Climate</a>
                        <a href="leadership.php">Economic Leadership Quiz</a>
                        <a href="economic.php">Economic Survey</a>
                        <a href="investor-survey.php">Investor Survey</a>
                        <a href="science.php">Science and Technology</a>
                        <a href="valuation.php">Valuation</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End -->
</div>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script defer src="js/fontawesome-all.js"></script>
    <script type="text/javascript" src="main.js"></script>
    <script type="text/javascript" src="js/jquery.imagebox.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.11/js/intlTelInput.min.js"></script>
    <script type="text/javascript">
        $(".main").imageBox();
    </script>
    <script type="text/javascript">

    //Dropdown
        $('.dropdown-menu', this).css('margin-top', 0);
        $('.dropdown').hover(function () {
            $('.dropdown-toggle', this).trigger('click').toggleClass("disabled");
        });
    //Scroll
        window.onscroll = function () { scrollFunction() };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("myBtn").style.display = "block";
            } else {
                document.getElementById("myBtn").style.display = "none";
            }
        }

    // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }

    //Videos
        $('video').bind('click', function (e) {
        let url = this.src;
        $('#modal1').children().children().attr("src",url);
        $('#modal1').modal('open',function (e) {
            complete:  $('#player').get(0).pause();
        });
         $('#player').get(0).play();
        });


    // <!-- Chat -->


        function add_chatinline() {
            var hccid = 28811207;
            var nt = document.createElement("script"); nt.async = true; nt.src = "https://mylivechat.com/chatinline.aspx?hccid=" + hccid;
            var ct = document.getElementsByTagName("script")[0]; ct.parentNode.insertBefore(nt, ct);
        }
        add_chatinline();

    // <!-- Form -->


    function attachHandler(el, evtname, fn) {
        if (el.addEventListener) {
            el.addEventListener(evtname, fn.bind(el), false);
        } else if (el.attachEvent) {
            el.attachEvent('on' + evtname, fn.bind(el));
        }
    }

        $('input[name=first_name]').focusout(function(){
            if(this.value == ""){
            this.style.border = "2px solid red";
            }else{
                this.style.border = "1px solid lightgray";
            }
        });
        $('input[name=last_name]').focusout(function(){
            if(this.value == ""){
            this.style.border = "2px solid red";
            }else{
                this.style.border = "1px solid lightgray";
            }
        });
        $('input[name=user_name]').focusout(function(){
            if(this.value == ""){
            this.style.border = "2px solid red";
            }else{
                this.style.border = "1px solid lightgray";
            }
        });
        $('input[name=password]').focusout(function(){
            if(this.value == ""){
            this.style.border = "2px solid red";
            }else{
                this.style.border = "1px solid lightgray";
            }
        });
        $('input[name=confirm_password]').focusout(function(){
            if(this.value == ""){
            this.style.border = "2px solid red";
            }else{
                this.style.border = "1px solid lightgray";
            }
        });


        document.addEventListener("DOMContentLoaded", function() {

        // JavaScript form validation

            var checkPassword = function(str)
            {
            var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
            return re.test(str);
            };

            var checkForm = function(e)
            {
            if(this.user_name.value == "") {
                alert("Error: Username cannot be blank!");
                this.user_name.focus();
                e.preventDefault(); // equivalent to return false
                return;
            }
            re = /^\w+$/;
            if(!re.test(this.user_name.value)) {
                alert("Error: Username must contain only letters, numbers and underscores!");
                this.user_name.focus();
                e.preventDefault();
                return;
            }
            if(this.password.value != "" && this.password.value == this.confirm_password.value) {
                if(!checkPassword(this.password.value)) {
                alert("The password you have entered is not valid!");
                this.password.focus();
                e.preventDefault();
                return;
                }
            } else {
                document.getElementById('error').innerHTML='Password not matched !!!';
                document.getElementById("inputConfirm").style.border = "2px solid red";
                this.confirm_password.focus();
                e.preventDefault();
                return;
            }
            };

            var myForm = document.getElementById("myForm");
            myForm.addEventListener("submit", checkForm, true);

        // HTML5 form validation

            var supports_input_validity = function()
            {
            var i = document.createElement("input");
            return "setCustomValidity" in i;
            }

            if(supports_input_validity()) {
            var user_nameInput = document.getElementById("inputUser");
            user_nameInput.setCustomValidity(user_nameInput.title);

            var pwd1Input = document.getElementById("inputPass");
            pwd1Input.setCustomValidity(pwd1Input.title);

            var pwd2Input = document.getElementById("inputConfirm");

        // input key handlers

            user_nameInput.addEventListener("keyup", function(e) {
                user_nameInput.setCustomValidity(this.validity.patternMismatch ? user_nameInput.title : "");
            }, false);

            pwd1Input.addEventListener("keyup", function(e) {
                this.setCustomValidity(this.validity.patternMismatch ? pwd1Input.title : "");
                if(this.checkValidity()) {
                pwd2Input.pattern = RegExp.escape(this.value);
                pwd2Input.setCustomValidity(pwd2Input.title);
                } else {
                pwd2Input.pattern = this.pattern;
                pwd2Input.setCustomValidity("");
                }
            }, false);

            pwd2Input.addEventListener("keyup", function(e) {
                this.setCustomValidity(this.validity.patternMismatch ? pwd2Input.title : "");
            }, false);

            }

            }, false);

    // <!-- Birthday -->

        function call(){
        var kcyear = document.getElementsByName("year")[0],
        kcmonth = document.getElementsByName("month")[0],
        kcday = document.getElementsByName("day")[0];
            
        var d = new Date();
        var n = d.getFullYear();
        for (var i = n; i >= 1950; i--) {
        var opt = new Option();
        opt.value = opt.text = i;
        kcyear.add(opt);
            }
        kcyear.addEventListener("change", validate_date);
        kcmonth.addEventListener("change", validate_date);

        function validate_date() {
        var y = +kcyear.value, m = kcmonth.value, d = kcday.value;
        if (m === "2")
            var mlength = 28 + (!(y & 3) && ((y % 100) !== 0 || !(y & 15)));
        else var mlength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m - 1];
        kcday.length = 0;
        for (var i = 1; i <= mlength; i++) {
            var opt = new Option();
            opt.value = opt.text = i;
            if (i == d) opt.selected = true;
            kcday.add(opt);
        }
            }
            validate_date();
        }
        
    // Phone Number
        $(document).ready(function() {
            $("#phone_number").intlTelInput();
        });
       

    // <!-- Gender -->

        function validate() {
            var a = 0, rdbtn=document.getElementsByName("gender")
                for(i=0;i<rdbtn.length;i++) {
                if(rdbtn.item(i).checked == false) {
                a++;
                }
            }
            if(a == rdbtn.length) {
            document.getElementById('error-gender').innerHTML='Please Select your Gender !!!';
            document.getElementById("gender").style.border = "2px solid red";
            return false;
            } else {
            document.getElementById("gender").style.border = "";
            }
        }
        
    //Video
    </script>

    
</body>

</html>
