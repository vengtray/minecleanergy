<?php include("header.php"); ?>

    <div id="contact">
        <div class="container">
            <div class="row">
                <div class="col-1 col-md-1 col-sm-1"></div>
                <div class="col-10 col-md-10 col-sm-10">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font center">
                                <h1>Contact Us Now !!!</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-4 col-sm-4">
                            <div class="contact-social center">
                                <a href="tel:+85510440214" class="white-font">
                                    <i class="fa fa-phone fa-4x" aria-hidden="true"></i>
                                    <br/>
                                    <br/>
                                    <p class="gold-font">Call us</p>
                                </a>
                                <p class="gold-font">We're here to help</p>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 col-sm-4">
                            <div class="contact-social center">
                                <a href="https://forum.aoecoin.io" class="white-font"  target="_blank">
                                    <i class="fa fa-users fa-4x" aria-hidden="true"></i>
                                    <br/>
                                    <br/>
                                    <p class="gold-font">Ask the community</p>
                                </a>
                                <p class="gold-font">981 members are online right now.</p>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 col-sm-4">
                            <div class="contact-social center">
                                <a href="mailto:ctw@aomegaenergy.com" class="white-font">
                                    <i class="far fa-envelope fa-4x" aria-hidden="true"></i>
                                    <br/>
                                    <br/>
                                    <p class="gold-font">Email us</p>
                                </a>
                                <p class="gold-font">Log-in required.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-1 col-md-1 col-sm-1"></div>
            </div>
        </div>
    </div>


<?php include("footer.php"); ?>