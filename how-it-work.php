<?php include("header.php"); ?>

    <div id="how-it-work">
        <div class="container">
            <div class="row">
                <div class="col-1 col-sm-1 col-md-1"></div>
                <div class="col-10 col-sm-10 col-md-10">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="title gold-font center">
                                <h3>HOW IT WORKS?</h3>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-12 col-sm-4 col-md-4">
                            <div class="title gold-font center">
                                <h4>HARDWARE</h4>
                                <div class="image">
                                    <img class="img-circle" src="img/hardware.jpg" alt="Hardware">
                                </div>
                                <div class="desc">
                                    Miner is a highly efficient piece of mining equipment specially designed for cryptocurrency mining. Our datacenters house
                                    hundreds of miners.
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-md-4">
                            <div class="title gold-font center">
                                <h4>POOLS</h4>
                                <div class="image">
                                    <img class="img-circle" src="img/pool.jpg" alt="Hardware">
                                </div>
                                <div class="desc">
                                    Miners are connected to pools. There are many pools so MineCleanergy allows you to connect to ones you choose. This allows
                                    you to find the most profitable combinantion.
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-md-4">
                            <div class="title gold-font center">
                                <h4>PAYOUT</h4>
                                <div class="image">
                                    <img class="img-circle" src="img/payout.jpg" alt="Hardware">
                                </div>
                                <div class="desc">
                                    Next, all mined cryptocurrency is distributed among all customers of MineCleanergy depending on their share of hashrate in
                                    the whole system.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-1 col-sm-1 col-md-1"></div>
            </div>
        </div>
    </div>

<?php include("footer.php"); ?>