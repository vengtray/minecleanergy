<?php include("header.php"); ?>
    <div class="row">
            <div class="col-12 col-sm-12 col-md-12">
                <div class="title gold-font">
                    <h1>AOE Cloud Mining</h1>
                    <p>The #1 Breakthrough Clean Energy Technology Startup in The World, now on the Blockchain! 1,920 All New
                        Breakthough Clean Energy Technologies and Commercializing them into Clean Energy Power Plants beginning
                        with the #1 Fuel Cell in the world for 24 Hours Clean Industrial and Commercial Base Load Power!!</p>
                    <p> You can Participate by Investing Now in the commercialization of all of these Breakthrough Technologies
                        and send humanity into the entire new Alpha Omega Energy era of Development Future Freedom Unprecedented!</p>
                    <p>Investors will Earn Steady Stable Income and Fantastic Growth in our World Leading Breakthrough Clean
                        Energy token, AOECOIN. Invest Now, and You Can Help US Change The World Together!</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Feature -->
    <div id="feature">
        <div class="container gold-font">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="title gold-font">
                        <h3>OUR FEATURES ARE UNBEATABLE</h3>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-1 col-sm-1 col-md-1"></div>
                <div class="col-10 col-sm-10 col-md-10">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <h3>INSTANT CONNECT</h3>
                            <img src="img/feature/plug.png" class="img-fluid">
                            <p>The mining starts immediately after confirmed payment. First payouts within 24 hours.</p>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <h3>INSTANT WITHDRAWAL</h3>
                            <img src="img/feature/btc.png" class="img-fluid">
                            <p>Choose the amount to withdraw and receive it instantly</p>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <h3>DETAILED STATISTICS</h3>
                            <img src="img/feature/area-chart.png" class="img-fluid">
                            <p>View all mining related information in real-time, at any point from any location.</p>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <h3>POOL ALLOCATION</h3>
                            <img src="img/feature/cogs.png" class="img-fluid">
                            <p>You can decide which pools you want your hashrate to mine in. This allows you to find the most profitable
                                combination.
                            </p>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <h3>FIXED FEES</h3>
                            <img src="img/feature/calculator.png" class="img-fluid">
                            <p>No hidden fees or comissions. Every single transaction is visible to you.</p>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4"></div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <p class="center">Our service makes mining Cryptocurrency accessible to everyone. No longer it is required to buy expensive
                                equipment and waste your time on setting it up. Simply select the desired power and generate revenue!</p>
                        </div>
                    </div>
                </div>
                <div class="col-1 col-sm-1 col-md-1"></div>
            </div>
        </div>
    </div>

    <!-- End -->

    <!-- Buy Cloud -->
    <div id="cloud">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="title gold-font center">
                        <h3>FIND A CLOUD THAT'S RIGHT FOR YOU</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-1 col-sm-1 col-md-1"></div>
                <div class="col-10 col-sm-10 col-md-10">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6">
                            <ul class="price-table">
                                <li class="title">
                                    Scrypt Cloud Mining
                                </li>
                                <li class="description">
                                    <strong>Scrypt Algorithm Miner</strong>
                                </li>
                                <li class="bullet-item">
                                    Minimum Hashrate: 1MH/s
                                </li>
                                <li class="bullet-item">
                                    Maintenance fee: 0.005 $ / 1 MH/s / 24h
                                </li>
                                <li class="bullet-item">
                                    Hardware: HashCoins SCRYPT
                                </li>
                                <li class="bullet-item">
                                    Automatic payout in BTC
                                </li>
                                <li class="bullet-item">
                                    In Stock
                                </li>
                                <li class="bullet-item">
                                    1 year contrac
                                </li>
                                <li class="price">
                                    <span>$4.20</span>
                                    <br/> per 1 MH/s
                                </li>
                                <li class="cta-button">
                                    <p>
                                        <span>
                                            <a href="#" class="btn btn-green" data-toggle="modal" data-target="#exampleModal">Buy Now</a>
                                        </span>
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6">
                            <ul class="price-table" style="margin-left:0%;">
                                <li class="title">
                                    SHA-256 CLOUD MINING
                                </li>
                                <li class="description">
                                    <strong>SHA-256 ALGORITHM MINER</strong>
                                </li>
                                <li class="bullet-item">
                                    Minimum Hashrate: 10 GH/s
                                </li>
                                <li class="bullet-item">
                                    Maintenance fee: 0.0035 $ / 10 GH/s / 24h
                                </li>
                                <li class="bullet-item">
                                    Hardware: HashCoins SHA-256
                                </li>
                                <li class="bullet-item">
                                    Automatic payout in BTC
                                </li>
                                <li class="bullet-item">
                                    In Stock
                                </li>
                                <li class="bullet-item">
                                    1 year contrac
                                </li>
                                <li class="price">
                                    <span>$1.80</span>
                                    <br/> per 10 GH/s
                                </li>
                                <li class="cta-button">
                                    <p>
                                        <span>
                                            <a href="#" class="btn btn-green" data-toggle="modal" data-target="#exampleModal">Buy Now</a>
                                        </span>
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-4 col-md-4">
                            <ul class="price-table">
                                <li class="title">
                                    ETHEREUM CLOUD MINING
                                </li>
                                <li class="description">
                                    <strong>ETHASH ALGORITHM MINER</strong>
                                </li>
                                <li class="bullet-item">
                                    Minimum Hashrate: 100 KH/s
                                </li>
                                <li class="bullet-item">
                                    Maintenance fee: NONE
                                </li>
                                <li class="bullet-item">
                                    Hardware: GPU Rigs
                                </li>
                                <li class="bullet-item">
                                    Automatic payout in ETH
                                </li>
                                <li class="bullet-item">
                                    In Stock
                                </li>
                                <li class="bullet-item">
                                    1 year contrac
                                </li>
                                <li class="price">
                                    <span>$2.20</span>
                                    <br/> per 100 KH/s
                                </li>
                                <li class="cta-button">
                                    <p>
                                        <span>
                                            <a href="#" class="btn btn-green" data-toggle="modal" data-target="#exampleModal">Buy Now</a>
                                        </span>
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-4 col-md-4">
                            <ul class="price-table">
                                <li class="title">
                                    ZCASH CLOUD MINING
                                </li>
                                <li class="description">
                                    <strong>EQUIHASH ALGORITHM MINER</strong>
                                </li>
                                <li class="bullet-item">
                                    Minimum Hashrate: 1 H/s
                                </li>
                                <li class="bullet-item">
                                    Maintenance fee: NONE
                                </li>
                                <li class="bullet-item">
                                    Hardware: GPU Rigs
                                </li>
                                <li class="bullet-item">
                                    Automatic payout in ZEC
                                </li>
                                <li class="bullet-item">
                                    In Stock
                                </li>
                                <li class="bullet-item">
                                    1 year contrac
                                </li>
                                <li class="price">
                                    <span>$2.00</span>
                                    <br/> per 1 H/s
                                </li>
                                <li class="cta-button">
                                    <p>
                                        <span>
                                            <a href="#" class="btn btn-green" data-toggle="modal" data-target="#exampleModal">Buy Now</a>
                                        </span>
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-4 col-md-4">
                            <ul class="price-table">
                                <li class="title">
                                    DASH CLOUD MINING
                                </li>
                                <li class="description">
                                    <strong>X11 ALGORITHM MINER</strong>
                                </li>
                                <li class="bullet-item">
                                    Minimum Hashrate: 1 MH/s
                                </li>
                                <li class="bullet-item">
                                    Maintenance fee: NONE
                                </li>
                                <li class="bullet-item">
                                    Hardware: Multi-Factor
                                </li>
                                <li class="bullet-item">
                                    Automatic payout in DASH
                                </li>
                                <li class="bullet-item">
                                    In Stock
                                </li>
                                <li class="bullet-item">
                                    1 year contrac
                                </li>
                                <li class="price">
                                    <span>$3.20</span>
                                    <br/> per 1 MH/s
                                </li>
                                <li class="cta-button">
                                    <p>
                                        <span>
                                            <a href="#" class="btn btn-green" data-toggle="modal" data-target="#exampleModal">Buy Now</a>
                                        </span>
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-1 col-sm-1 col-md-1"></div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="title gold-font">
                        <p>Select the desired plan and start earning in less than 24 hours!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <img src="img/aoelogo.png" alt="#" class="img-fluid">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="errors"></div>
                <form action="index.php" method="POST" id="myForm" onsubmit="return validate()">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="inputFirstName">First Name</label>
                                    <input type="text" name="first_name" class="form-control" id="inputFirstName" placeholder="Your First Name" required pattern="\w+">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="inputLastName">Last Name</label>
                                    <input type="text" name="last_Name" class="form-control" id="inputLastName" placeholder="Your Last Name" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="inputUser">Username</label>
                                    <input type="text" name="user_name" class="form-control" id="inputUser" placeholder="Username" required pattern="\w+">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="inputPass">Password</label>
                                    <input type="password" name="password" class="form-control" id="inputPass" placeholder="Password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="inputConfirm">Confirm Password</label>
                                    <input type="password" name="confirm_password" class="form-control" id="inputConfirm" placeholder="Password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}">
                                    <span id="error" style="color:#F00; font-size: 14px;"> </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label>Birthday</label>
                                    <br/>
                                    <label for="month" style="font-size: 18px;">Month :</label>
                                    <select name="month" id="month" onchange="call()" style="font-size: 18px;" required>
                                        <option value="">select</option>
                                        <option value="1">Jan</option>
                                        <option value="2">Feb</option>
                                        <option value="3">Mar</option>
                                        <option value="4">Apr</option>
                                        <option value="5">May</option>
                                        <option value="6">Jun</option>
                                        <option value="7">Jul</option>
                                        <option value="8">Aug</option>
                                        <option value="9">Sep</option>
                                        <option value="10">Oct</option>
                                        <option value="11">Nov</option>
                                        <option value="12">Dec</option>
                                    </select>
                                    <label for="day" style="font-size: 18px;">Day :</label>
                                    <select name="day" id="day" style="font-size: 18px;" required>
                                        <option value="">select</option>
                                    </select>
                                    <label for="year" style="font-size: 18px;">Year :</label>
                                    <select name="year" id="year" onchange="call()" style="font-size: 18px;" required>
                                        <option value="">select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12">
                                <span id="gender" style="margin-left: 20px;">
                                    Gender:
                                <input type="radio" name="gender" value="male" style="margin-left: 10px; transform: scale(1.5);"> Male
                                <input type="radio" name="gender" value="female" style="margin-left: 10px; transform: scale(1.5);"> Female
                                </span>
                                <br/>
                                <span id="error-gender" style="color:#F00; font-size: 14px; margin-left: 20px;"> </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12">
                                <div class="input-group" style="margin-left:25px;">
                                    <label for="phone_number">Phone Number :</label>
                                    <input type="text" id="phone_number" name="phone_number" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <input type="submit" name="submit" class="btn btn-primary"></input>
                    </div>                
                </form>
                <div id="verify_code" style="display: none;">
                    <p>Calling you now.</p>
                    <p>When prompted, enter the verification code:</p>
                    <h1 id="verification_code"></h1>
                    <p><strong id="status">Waiting...</strong></p>
                </div>
            </div>
        </div>
    </div>
<?php include("footer.php"); ?>