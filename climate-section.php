<?php include("header.php");?>
    
    <div id="climate-section">
        <div class="container">
            <div class="row">
                <div class="col-1 col-sm-1 col-md-1"></div>
                <div class="col-10 col-sm-10 col-md-10">
                    <div class="title">
                        <h1>Climate</h1>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="pictures">
                                <img src="img/DcX4P4sX4AY8j-l.jpg" alt="" class="img img-test" data-url="img/DcX4P4sX4AY8j-l.jpg">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="pictures">
                                <img src="img/DcVO_QDVwAAlLoc.jpg" alt="" class="img img-test" data-url="img/DcVO_QDVwAAlLoc.jpg">
                            </div>
                        </div>
                    </div>
                    <div class="title" style="margin-top:50px;">
                        <h1>
                            CALL CLIMATE CHANGE WHAT IT IS: VIOLENCE
                        </h1>
                    </div>
                    <div class="text gold-font">
                        <p>SOCIAL UNREST AND FAMINE, SUPERSTORMS AND DROUGHTS. PLACES, SPECIES AND HUMAN BEINGS – NONE WILL BE SPARED. WELCOME TO OCCUPY EARTH</p>
                    </div>
                    <div class="pictures">
                        <img src="img/climate.jpeg" alt="">
                    </div>
                    <div class="link">
                        <a href="https://www.theguardian.com/commentisfree/2014/apr/07/climate-change-violence-occupy-earth?CMP=share_btn_tw" type="button" class="btn btn-success" style="color: blue;">Read More</a>
                    </div>
                </div>
                <div class="col-1 col-sm-1 col-md-1"></div>
            </div>
        </div>
    </div>

<?php include("footer.php");?>